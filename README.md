# README #

Please, if you use this software contact research@dpastoresc.org for further information. Apologies for the lack of straightforward documentation.

Collaborators are welcome too.

Also, visit https://sourceforge.net/projects/mia/ for details on the B-splines registration engine running under this workflow (the library is properly delivered and documented)

This source code includes also 3rd party libraries open source (see license and readme when needed)

Thanks!