%David Pastor Escuredo (BIT, UCSD) 2012
%Script to read tif images from miscroscope LAICA and generate experiments
%for forces measerument. Reference and Deformed images are ANALYZE FORMAT
%to work with mia code

%Set the dataset folders
clear
datas='121017'
folder = ['D:\\BIT\121017 bad day/TIFF'];
folderOut=['D:\\BIT\' datas '\'];
%Relaxed state
ref = 'Series027'

%One dataset for the session. arbitrary name
%out = 'Series018' %if it is not the same than set
%z slices. should automate this
zed = 244
steps = 0
channel=[0,1];%we have ch 3 also
%Dataset parameters
si=[1024 1024 zed];
%sp=[0.181 0.181 0.25];
sp=[0.434 0.434 0.25];
sampling=[10 10 10];
datatype=2;%'uint8' in hdr type 
origin=[0 0 0];

%Package to work with analyze data
addpath('NIFTI/') 
out=ref;

%files = dir(target);
%s= size(files);
size(channel)

for c=1:size(channel,2)
    ch=channel(c)
    count=0
    %rmdir([folder set tag '\ch0' num2str(ch) '\']);
    outFolder=[folderOut out '_ch0' num2str(ch) '/']
    mkdir(outFolder);

   % for t =0:steps
        for z = 0:zed
        
          slice = '_z';
          if(z>99)
              slice = [slice num2str(z)];
          elseif(z>9)
              slice = [slice '0' num2str(z)];
          else
              slice = [slice '00' num2str(z)];
          end
          %Create reference and deformed images as t000 and t001 steps.
          %easier to understand
          refIm = [folder '\' ref slice '_ch0' num2str(ch) '.tif'];
          %defIm = [folder def '\' def slice '_ch0' num2str(ch) '.tif'];
          %read tiff slices
          A=imread(refIm, 'TIFF');
          %A1=imread(defIm, 'TIFF');  
          %max(max(A))
          %size(A)
          B(:,:,z+1)=A;
          %B1(:,:,z+1)=A1;
          count=count+1;
       end
       count
       %make images byte precision
       Bb= Long2Byte(B);
       %Bb1= Long2Byte(B1);
       %use nifti package to convert to analyze
       ana = make_ana(Bb, sp, origin, datatype);
       clear Bb;
       %ana1 = make_ana(Bb1, sp, origin, datatype);
       %clear Bb1;
       desRef = [outFolder out '_t000_ch0' num2str(ch) ]
       %desDef = [outFolder out '_t001_ch0' num2str(ch) ]
       save_untouch_nii(ana, desRef);
       %save_untouch_nii(ana1, desDef);
       'converted to analyze'
       %clear ana1;
       clear ana;
    %end
    %end
end

%Create a mesh of points
origPoints = [outFolder out '_' num2str(sampling(1)) '_' num2str(sampling(2)) '_' num2str(sampling(3)) '.csv']
gridPointsCSV(origPoints, si, sampling);     