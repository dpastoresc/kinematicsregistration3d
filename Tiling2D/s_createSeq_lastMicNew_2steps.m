%David Pastor Escuredo (BIT, UCSD) 2012
%Script to read tif images from miscroscope LAICA and generate experiments
%for forces measerument. Reference and Deformed images are ANALYZE FORMAT
%to work with mia code

%ucsdDatasetParameters;

%Package to work with analyze data
addpath('../libs/NIFTI/') 
addpath('../libs/utils/') 

%files = dir(target);
%s= size(files);
size(channel)
clear B;
clear Bb;
for c=1:size(channel,2)
    ch=channel(c)
    count=0
    %rmdir([folder set tag '\ch0' num2str(ch) '\']);
    outFolder=[folderOut out '_ch0' num2str(ch) '/']
    mkdir(outFolder);

    for t =0:steps
        for z = 0:zed
          time = '_t';
          z
          if(t>9)
              time = [time '0' num2str(t)];
          else
              time = [time '00' num2str(t)];
          end
          slice = '_z';
          if(z>99)
              slice = [slice num2str(z)];
          elseif(z>9)
              slice = [slice '0' num2str(z)];
          else
              slice = [slice '00' num2str(z)];
          end
          %Create reference and deformed images as t000 and t001 steps.
          %easier to understand
          %refIm = [folder ref '\' ref slice '_ch0' num2str(ch) '.tif'];
          %defIm = [folder def '\' def slice '_ch0' num2str(ch) '.tif'];
          
          refIm = [folder  ref slice '_ch0' num2str(ch) '.tif'];
          defIm = [folder def slice '_ch0' num2str(ch) '.tif']
          %read tiff slices
          A=imread(refIm, 'TIFF');
          A1=imread(defIm, 'TIFF');  
          %max(max(A))
          %size(A)
          B(:,:,z+1)=A;
          B1(:,:,z+1)=A1;
          count=count+1;
       end
       count
       %make images byte precision
       Bb= Long2Byte(B);
       Bb1= Long2Byte(B1);
       clear B;
       clear B1;
       %use nifti package to convert to analyze
       ana = make_ana(Bb, sp, origin, datatype);
       clear Bb;
       ana1 = make_ana(Bb1, sp, origin, datatype);
       clear Bb1;
       desRef = [outFolder out '_t000_ch0' num2str(ch) ]
       desDef = [outFolder out '_t001_ch0' num2str(ch) ]
       save_untouch_nii(ana, desRef);
       save_untouch_nii(ana1, desDef);
       'converted to analyze'
       clear ana1;
       clear ana;
    end
    %end
end

%FIX ME: Create a mesh of points
%origPoints = [outFolder out '_' num2str(sampling(1)) '_' num2str(sampling(2)) '_' num2str(sampling(3)) '.csv']
%gridPointsCSV(origPoints, si, sampling);     


