%Set the dataset folders
clear
datas='130123'
x4=0
home='Y:\zForces3D\data\';
folder = [home datas '\original\'];
folderOut=[home datas '\'];
%Relaxed state-> creates the timestep
ref = 'Series038'
%Stressed state
def = 'Series034'
%One dataset for the session. arbitrary name
out = 'E1' %if it is not the same than set
%z slices. should automate this
zed = 244
steps = 0
channel=[0,1];%we have ch 3 also
%Dataset parameters
si=[1024 1024 zed];
if x4
    out=[out 'A']
sp=[0.181 0.181 0.25];
else
    out=[out 'B']
sp=[0.42 0.42 0.25];
end
sampling=[10 10 10];
datatype=2;%'uint8' in hdr type 
origin=[0 0 0];