% /* --------------------------------------------------------------------------------------
%  * File:    configFileSystem.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.

if sim
   if forwards>0
        outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim_regFwd/'];
        outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim/bwd/'];
   else
        outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim_regBwd/'];
        outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim/fwd/'];
   end 
else
    if forwards>0
        outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_regFwd/'];
        outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '/bwd/'];
    else
        outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_regBwd/'];
        outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '/fwd/'];
    end
end
outfoldertemp= [outfolder];
mkdir(outfolder)
mkdir(outfoldertemp)