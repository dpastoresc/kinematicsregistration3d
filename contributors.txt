%  * Author:  David Pastor Escuredo, research@dpastoresc.org
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2013-2014-2015-2016, David Pastor Escuredo - research@dpastoresc.org
%  with BIT-UPM, UPM, CNRS and UCSD
%  All rights reserved.