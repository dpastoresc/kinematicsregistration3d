% /* --------------------------------------------------------------------------------------
%  * File:    evaluateSplines.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.

% Formats:
%    - list of points(centers) 3D -> keep in .csv
%           id;t;x;y;z;reserved
%
%    - vector field sparse 3D -> keep in .csv
%           id;t;x;y;z;t;u;v;w;reserved
%
%    - tensor file format tff
%           MIA FORMAT check file formats documents and see header
%           readTensorFile.m (x,y,z,t) ({tensor})
%
%    - simple tensor descriptor file .csv
%           id;t;x;y;z;e1;angle1;e2;angle2;e3;angle3
%
%    - .emb cell tracking
%           cellid;cellid;x;y;z;t;motherRef;reserved
%           where motherRef is the index of the mother within the step
%
%    - .csv cell selection
%           cellid;sel;t;x;y;z;cellmotherid;reserved
%
%
% ToDO:
%   - Check the order of tin and tref for registration and tracking
%   - GetDetection and GenerateMesh should be better and with input parameters
%   - BACKWARDS one
%   - calculate trajectories by point and not by timestep
%   - ID of the trajectory is not preserved in the tensor file!!! FIXED

% READ!!!! the registration from t->t+1 generates an image that converts
% the step t into an image more similar to t+1 (reference image)
% However!! the evaluation of pixel movements has to be done in the other
% way (inverse interpolation) so to track cells from t->t+n we need to
% register the sequence from t+n->t. The velocity field is defined here
% from t+1->t

% READ!!!! the registration from t+1->t generates an image that converts
% the step t+1 into an image more similar to t (reference image)
% However!! the evaluation of pixel movements has to be done in the other
% way (inverse interpolation) so thus we track cells from t->t+n

% IMPORTANT!!!
% Convention:
%forwards
%t000->t001 generates t000.v3dt and t000_reg.image
%backwards
%We alter the order of tref and tin
%t001->t000 generates t001.v3dt and t001_reg.image
%If we want to evaluate then forwards the tracking
%t000 points use bwds/t001.v3dt to move to t001
%If tracking backwards
%t001 points use fwds/t000.v3dt to move to t000
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loadParametersEvalSplines;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% AUTOMATED CODE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% use the first image to init dataset images parameters %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
stamp_ini=makeTimeStamp(tini);
firstimage=[folder dataset tag stamp_ini ch format];
addpath('ReadData3D/')
[Im sin sp]=loadImage(firstimage);
[Im iinf]=ReadData3D(firstimage);
size(Im)
if physical
    spX=sp(1);
    spY=sp(2);
    spZ=sp(3);
else
    spX=1;spY=1;spZ=1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% The original seed o points in the first frame: %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

snodetag=['_snode' num2str(snode(1)) '_' num2str(snode(2)) '_' num2str(snode(3))];

if sim
    mesh=[outfolder2 dataset tag stamp_ini ch snodetag '.csv']
else
    mesh=[outfolder dataset tag stamp_ini ch snodetag '.csv']
end

mesh=[outfolder dataset stamp_ini ch snodetag '.csv']
readMask=0
%name setting for tracking and tensors initialpos
%vf is always a mesh
if seedsMode==0
    %This is good for regular mesh tracking estimation
    initialpos=mesh;
elseif seedsMode==1
    %fast detection
    initialpos=[outfolder dataset stamp_ini ch '.csv']
    getDetection(initialpos, Im, tini);
elseif seedsMode==2
    %detection from bioemergences platform
    initialpos=[outfolder dataset stamp_ini ch '.csv']
    readBioemergencesDetection(initialpos, bioemergencesCentersVTK, tini)
elseif seedsMode==3
    %selection from movit
    initialpos=[outfolder dataset stamp_ini ch '.csv']
    %readMovitSelection(initialpos, movitSelection, tini);TO
    %IMPLEMENT
elseif seedsMode==4
    %mesh with mask
    mesh=[outfolder dataset stamp_ini ch snodetag  '_masked.csv']
    initialpos=mesh;
    readMask=1
elseif seedsMode>4 && seedsMode<6
    %mask for tracking (5-membranes) (6-nuclei)
    mesh=[outfolder dataset stamp_ini ch snodetag  '_masked.csv']
    initialpos=[outfolder dataset stamp_ini ch '.csv'];
    readMask=1
elseif seedsMode==6
    initialpos=[outfolder dataset stamp_ini ch '.csv'];
    readMask=0;
    A=dlmread(mySeeds,';');
    ix=(find(A,5)>-1);
    A=A(ix,:);
    A(:,5)=0;
    dlmwrite(initialpos, A,';');
    %pointsTCSV_2D(initialpos, tini, mask)
elseif seedsMode==8
    initialpos=squaredGridFile
    grid=[outfolder dataset stamp_ini ch snodetag '_squared.csv']
    gridSquaresTCSV(mesh, sin , snode, tini);
elseif seedsMode==9
    initialpos=squaredGridFile
    grid=[outfolder dataset stamp_ini ch snodetag '_vertical.csv']
    gridVerTCSV(mesh, sin , snode, tini);
elseif seedsMode==10
    initialpos=squaredGridFile
    grid=[outfolder dataset stamp_ini ch snodetag '_horizontal.csv']
    gridHorTCSV(mesh, sin , snode, tini);
elseif seedsMode==11
    %python reading of centers
    A=dlmread(maskFile,' ');
    size(A)
    B=zeros(size(A,1),5);
    for i=1:size(A,1)
        B(i,:)=[i,tini,A(i,1), A(i,2), A(i,3)];
    end
    dlmwrite([maskFile '.csv'], B, ';');
    initialpos=[maskFile '.csv']
    readMask=0
elseif seedsMode==13
    maskFile=[contourFolder dataset makeTimeStamp(tini) '_ch01' format]
    mesh=[outfolder dataset makeTimeStamp(tini) ch snodetag '_masked.csv']
    mask=imread(maskFile);
    mask=mask';%CAAAAAAAAAAREEEEEEEEEFULLLLLLLLLLLLLLLLL
    mask=imfill(mask,'holes');
    %write mesh
    gridPointsMaskTCSV(mesh, sin , snode, tini, mask);
    
    %Pass the new mesh as X
    initialpos=mesh
    %fit timestep
    stepTransform=num2str(step)
    readMask=0
    doMask=0
end

%%%%%%%%%%%%%%%%%%%%% Mask, mesh generation%%%%%%%%%%%%%%%%%%%
if(doMask)
    Mask=Im;
    mmask=max(Mask(:))
    Mask(Mask<(mmask*backgroundThresholdRatio))=0;
    Mask(Mask>0)=255;
    Mask=medfilt3(Mask,[7 7 5]);
    %Mask=Mask';
    %Mask=imclose(Mask, strel('disk',4,0));
    %%imwrite(Mask, 'mask.tif');
    size(Mask)
    %figure;imagesc(Mask(:,size(Mask,3)/2));    
    gridPointsMaskTCSV(mesh, sin , snode, tini, Mask)
    clear Mask;
elseif readMask
    %create filled solid mask from membranes
    mask=imread(maskFile);
    mask=mask';%CAAAAAAAAAAREEEEEEEEEFULLLLLLLLLLLLLLLLL
    mask2=imfill(mask,'holes');
    gridPointsMaskTCSV(mesh, sin , snode, tini, mask);
    
    if seedsMode==5
        %create subsamples points / pseudo detection of structures
        mask=imerode(mask, strel('disk', 1, 0));
        %imshow(mask)
        pointsTCSVsubsampled(initialpos, tini, mask, 10)
    end
    if seedsMode==6
        %create subsamples points / pseudo detection of structures
        mask=imdilate(mask, strel('disk', 4, 0));
        %imshow(mask)
        pointsTCSVsubsampled(initialpos, tini, mask, 10)
    end
else
    gridPointsTCSV(mesh, sin , snode, tini);
end
clear mask
clear mask2
clear Im

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% EVAL BSPLINES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Reminder.....
% Convention:
%forwards
%t000->t001 generates t000.v3dt and t000_reg.image
%backwards
%We alter the order of tref and tin
%t001->t000 generates t001.v3dt and t001_reg.image
%If we want to evaluate then forwards the tracking
%t000 points use bwds/t001.v3dt to move to t001
%If tracking backwards
%t001 points use fwds/t000.v3dt to move to t000

for t=tini:step:(tfinal-1)
    
    tref=t+step;
    stampin = makeTimeStamp(t);
    stampref = makeTimeStamp(tref);
    namein=[folder datasetN stampin ch format]
    nameref=[folder datasetN stampref ch format]
    %namein=[folder dataset stampin ch format]
    %nameref=[folder dataset stampref ch format]
    if sim
        namet=[outfoldertemp datasetN stampref ch opt_tag '_sim' simtag '.v3dt']
        nameout=[outfoldertemp datasetN stampref ch opt_tag '_reg' format]
    else
        namet=[outfoldertemp datasetN stampref ch opt_tag '.v3dt']
        nameout=[outfoldertemp datasetN stampref ch opt_tag '_reg' format]
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % We use the seed points (detection or mesh) to track pixels over the deformation fields
    % We update the position every time to get the trajectory
    % ToNow it works by timesteps but should make it by trajectories too
    if doTracking
        
        if seedsMode>0
            ipos=[outfoldertrack datasetN stampin ch '.csv']
            pos=[outfoldertrack datasetN stampref ch '.csv']
        else
            ipos=[outfoldertrack datasetN stampin ch snodetag '.csv']
            pos=[outfoldertrack datasetN stampref ch snodetag '.csv']
        end
        
        if t==tini
            copyfile(initialpos,ipos);
        end
        
        command_track = [ exe_track ' -i ' ipos ...
            ' -o ' pos ' -t ' namet ' -T ' num2str(step)] ; %should have the step here!!
        tic; system(command_track); toc
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % As the previous one, but we dont update the seed, so we have a regular
    % mesh for all timepoints. For the rest of decriptors we prefer to get
    % the tracking :).
    if doVF
        ipos=[outfoldervf datasetN stampin ch snodetag '.csv']
        if t==tini
            copyfile(mesh,ipos);
        end
        pos=[outfoldervf datasetN stampref ch snodetag '.csv']
        
        command_vf = [ exe_track ' -i ' mesh ' -o ' pos ' -t ' namet ' -T ' num2str(t+step-tini)];
        tic; system(command_vf); toc
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Calculate tensors of strain E from the splines field
    % This is for the static mesh of points (static fields, only Eulerian)
    if doStrains
        %The input are trajectories with updated positions instead of fixed
        %mesh, so we can calcualte the Langrange descriptors
        if seedsMode>0
            ipos=[outfoldertrack datasetN stampin ch '.csv']
        else
            ipos=[outfoldertrack datasetN stampin ch snodetag '.csv']
        end
        
        tensor=[outfoldertensors dataset stampin ch '_strain.tff'] %Tensor file format .tff
        %we pass the points of trajectories in each point so we evaluate
        %the tensors along the pathlins
        %command_strain = [ exe_tensor ' -i ' mesh ' -t ' namet ' -o ' tensor '-quantity=strain'];!
        %OLD (mesh does not work for lagrange descriptors): we use initialpos to calculate derivatives only in the mesh and
        command_strain = [ exe_tensor ' -i ' ipos ' -t ' namet ' -o ' tensor ' -q strain'];!
        tic; system(command_strain); toc
    end
    
    %Calculate tensor of deformation from the splines field
    if doDeform
        %The input are trajectories with updated positions instead of fixed
        %mesh, so we can calcualte the Langrange descriptors
        if seedsMode>0
            ipos=[outfoldertrack datasetN stampin ch '.csv']
        else
            ipos=[outfoldertrack datasetN stampin ch snodetag '.csv']
        end
        
        tensor=[outfoldertensors dataset stampin ch '_deform.tff'] %Tensor file format .tff
        %we pass the points of trajectories in each point so we evaluate
        %the tensors along the pathlines
        command_stress = [ exe_tensor ' -i ' ipos ' -t ' namet ' -o ' tensor ' -q derivative'];
        tic; system(command_stress); toc
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Calculate tensor of deformation from the splines field updating the
    %trajectory position enabling for langrangian analysis
    if doDeformTrack
        ipos=[outfoldertrack datasetN stampin ch '.csv']
        tensor=[outfoldertracktensors datasetN stampin ch '_deform.tff'] %Tensor file format .tff
        %we pass the points of trajectories in each point so we evaluate
        %the tensors along the pathlines
        command_stress = [ exe_tensor ' -i ' ipos ' -t ' namet ' -o ' tensor ' -q derivative'];
        tic; system(command_stress); toc
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%% EXPORTING RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if exportTracking
    %Export tracking as EMB file for movit
    %This count is the cellnumber in emb files (first column)
    count=1;
    Tracking=zeros(0);
    embfile=[folder dataset ch opt_tag '.emb']
end
if exportTrackingWorkflow
    %Export tracking for Workflow
    %This count is the cell id. No cell number now...
    count2=1;
    Tracking2=zeros(0);
    TRACK_VALID=-1;
    CENTER_VALID=-1;
    trackingWorkflow=[folder dataset ch opt_tag '_workflow.emb']
    fid=fopen(trackingWorkflow, 'w');
    endfopen(trackingWorkflow, 'w');
end

if exportVF
    if sim
        if physical
            vffile=[outfolder2 datasetN ch '_' cnode_tag opt_tag '_vfphys' snodetag '_sim' simtag '.csv']
        else
            vffile=[outfolder2 datasetN ch '_' cnode_tag opt_tag '_vf' snodetag '_sim' simtag '.csv']
        end
    else
        if physical
            vffile=[outfolder2 datasetN ch '_' cnode_tag opt_tag '_vfphys' snodetag '.csv']
        else
            vffile=[outfolder2 datasetN ch '_' cnode_tag opt_tag '_vf' snodetag '.csv']
        end
    end
    if weights
        vffile=[vffile(1:length(vffile)-4) '_weights.csv']
        %fidweights=fopen(wpath, 'w');
    end
    fidVF=fopen(vffile, 'w');
end
if exportVFinverted
    if sim
        if physical
            vffile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_vfinvphys' snodetag '_sim' simtag '.csv']
        else
            vffile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_vfinv' snodetag '_sim' simtag '.csv']
        end
    else
        if physical
            vffile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_vfinvphys' snodetag '.csv']
        else
            vffile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_vfinv' snodetag '.csv']
        end
    end
    if weights
        vffile=[vffile(1:length(vffile)-4) '_weights.csv']
        %fidweights=fopen(wpath, 'w');
    end
    fidVFi=fopen(vffile, 'w');
end
if (exportDeform)
    descriptorsFile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_df' snodetag '.csv']
    if exportStrain
        descriptorsFile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_sf' snodetag '.csv']
    end
    fidDF=fopen(descriptorsFile, 'w')
end
if (exportStrains)
    descriptorsFile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_sf' snodetag '.csv']
    fidDF=fopen(descriptorsFile, 'w')
end
if exportEuler
    descriptorsFile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_eu' snodetag '.csv']
    fidDF=fopen(descriptorsFile, 'w')
    descriptorsFile_v2=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_ev' snodetag '.csv']
    fidDF_v2=fopen(descriptorsFile_v2, 'w')
end
if exportEulerTrack
    descriptorsFile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_et' snodetag '.csv']
    fidDFt=fopen(descriptorsFile, 'w')
    descriptorsFile_v2=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_ew' snodetag '.csv']
    fidDFt_v2=fopen(descriptorsFile_v2, 'w')
end
if exportLag
    descriptorsFile=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_lg' snodetag '_w' num2str(lapse) '.csv']
    fidLF=fopen(descriptorsFile, 'w')
    descriptorsFilee=[outfolder2 datasetN ch '_' num2str(cnode) opt_tag '_le' snodetag '_w' num2str(lapse) '.csv']
    fidLFe=fopen(descriptorsFilee, 'w')
end

for t=tini:step:tfinal
    t
    tini
    stampref = makeTimeStamp(t);
    %Export movit tracking
    if exportTracking
        
        if seedsMode>0
            pos=[outfoldertrack datasetN stampref ch '.csv']
        else
            pos=[outfoldertrack datasetN stampref ch snodetag '.csv']
        end
        stepD=dlmread(pos,';');
        %step(1:2,:)
        Tracking(count,:)=[0 0 0 0 0 t 0 0 0];
        count=count+1;
        countStep=1;
        stepData=zeros(0);
        if t==tini
            for i=1:size(stepD,1)
                stepData(countStep, :)= [count stepD(i,1) stepD(i,3) stepD(i,4) stepD(i,5) stepD(i,2) 0 count -1];
                %fprintf(fid, '%s', [num2str(count) ';' num2str(step(i,1)) ';' num2str(i,3) ';' num2str(step(i,4)) ';' num2str(step(i,5)) ';' num2str(step(i,2)) ';' num2str(step(i,1)) ';1'] );
                %fprintf(fid, '\n');
                count=count+1;
                countStep=countStep+1;
            end
        else
            for i=1:size(stepD,1)
                %oldstep(1:2,:)
                %step(i,1)
                motherid=find(oldstep(:,1)==stepD(i,1));
                stepData(countStep, :)=[count stepD(i,1) stepD(i,3) stepD(i,4) stepD(i,5) stepD(i,2) motherid count -1];
                %fprintf(fid, '%s', [num2str(count) ';' num2str(step(i,1)) ';' num2str(i,3) ';' num2str(step(i,4)) ';' num2str(step(i,5)) ';' num2str(step(i,2)) ';' num2str(step(i,1)) ';1'] );
                %fprintf(fid, '\n');
                count=count+1;
                countStep=countStep+1;
            end
        end
        oldstep=stepD;
        
        %the splines can take values out of the image... cut them!
        %for matlab image is between [1 size]
        in=stepData(:,3)>(sin(1));
        stepData(in,3)=sin(1);
        
        in=stepData(:,4)>(sin(2));
        stepData(in,4)=sin(2);
        
        in=stepData(:,5)>(sin(3));
        stepData(in,5)=sin(3);
        
        in=stepData(:,3)<1;
        stepData(in,3)=1;
        
        in=stepData(:,4)<1;
        stepData(in,4)=1;
        
        in=stepData(:,5)<1;
        stepData(in,5)=1;
        
        stepData(:,3:5)=stepData(:,3:5)-1;        
        stepData(:,3)=stepData(:,3)-(sin(1)/2);
        stepData(:,4)=stepData(:,4)-(sin(2)/2);
        stepData(:,5)=stepData(:,5)-(sin(3)/2);
        Tracking=vertcat(Tracking, stepData);
        count=size(Tracking,1)+1;
    end
    
    if exportTrackingWorkflow
        if seedsMode>0
            pos=[outfoldertrack datasetN stampref ch '.csv']
        else
            pos=[outfoldertrack datasetN stampref ch snodetag '.csv']
        end
        countStep2=1;
        stepIds=zeros(0);
        if t==tini
            for i=1:size(step,1)
                fprintf(fid, 'ID,TIME,MOTHER,X,Y,Z,TRACK_VALID,CENTER_VALID');
                fprintf(fid, '\n'); %Header
                %stepData(countStep2, :)= [count2 step(i,2) step(i,3) step(i,4) step(i,5) 0 -1];
                fprintf(fid,'%d,%d,%d,%f,%f,%f,%f,%f', count2, step(i,2), -1, step(i,3), step(i,4), step(i,5), TRACK_VALID, CENTER_VALID);
                fprintf(fid, '\n');                
                stepIds(countStep2,:)=[step(i,1) count2];%we keep the id we gave to the cell.
                countStep2=countStep2+1;
                count2=count2+1;
            end
        else
            for i=1:size(step,1)
                %find mother
                motherid2=find(oldIds(:,1)==step(i,1));                
                fprintf(fid,'%d,%d,%d,%f,%f,%f,%f,%f', count2, step(i,2), oldIds(motherid2,2) , step(i,3), step(i,4), step(i,5), TRACK_VALID, CENTER_VALID);
                fprintf(fid, '\n');                
                stepIds(countStep2,:)=[step(i,1) count2];%we keep the id we gave to the cell.
                count2=count2+1;
                countStep2=countStep2+1;
            end
            
        end
        oldIds=stepIds;
        %//SAVE GLOBAL FILE
        %void Embryo::SaveLineageTree(char * outputFile){
        %FILE *fp = fopen(outputFile,"w");
        %fprintf(fp,"ID,TIME,MOTHER,X,Y,Z,TRACK_VALID,CENTER_VALID\n"); //Header
        %for(int time=timeBegin;time<=timeEnd;time++){
        %	for(map<int,Cel* >::iterator it = TimeCenter[time].begin(); it!= TimeCenter[time].end(); ++it){
        %			Cel * c=(*it).second;
        %			int mother=-1; if(c->Mother!=NULL) mother=c->Mother->id;
        %			fprintf(fp,"%d,%d,%d,%f,%f,%f,%f,%f\n",c->id,c->time,mother,c->XYZ[0],c->XYZ[1],c->XYZ[2],c->TRACK_VALID,c->CENTER_VALID);
        %		}
        %	}%
        %	fclose(fp);%
        %	printf(" Save Lineage to %s \n",outputFile);
        %}
    end
    
    keepOn=1
    if t==tfinal
        keepOn=0
        %We write the last position of the VF to cover the end of the
        %tracking too.
        if exportVF
            fprintf(fidVF, '%s', ['0;' num2str(t) ';1;1;1;0;0;0;0']);
            fprintf(fidVF, '\n');
        end
    end
    
    if keepOn
        stampnext = makeTimeStamp(t+step);
        if exportVF
            %Format of VF for my GUI Visualization Framework Dynamics
            pos=[outfoldervf datasetN stamp_ini ch snodetag '.csv']
            lpos=[outfoldervf datasetN stampnext ch snodetag '.csv']
            origin=dlmread(pos,';');
            destin=dlmread(lpos,';');
            ss= destin(1,2)-step
            
            if weights
                %tref=t+step;
                %stampin = makeTimeStamp(t);
                %     stampref = makeTimeStamp(tref);
                %     namein=[folder dataset stampin ch format]
                %     nameref=[folder dataset stampref ch format]                             %
                %     namet=[outfoldertemp dataset stampref ch opt_tag '.v2dt']
                %     nameout=[outfoldertemp dataset stampref ch opt_tag '_reg' format]
                [Im sin sp]=loadImage(namein);
                if metricWeight
                    Im=double(Im);
                    [ImDef sin sp]=loadImage(nameout);
                    ImDef=double(ImDef);
                    
                    Im=(Im-ImDef).^2;
                end
                Im=double(Im);
                [Dx Dy Dz]=gradient(Im, spX, spY, spZ);
                WeightIm=Dx+Dy+Dz;
                h = fspecial3('average',[21 21 7]);
                Im=imfilter(Im,h);
                WeigthIm=WeightIm./Im;
                
                %fprintf(fidweights, '%s', [num2str(origin(l,1)) ';' num2str(desin(l,2)-1) ';' ww]);
                %fprintf(fidweights, '\n');
            end
            
            for l=1:size(origin,1)
                dX=destin(l,3)-origin(l,3);
                dY=destin(l,4)-origin(l,4);
                dZ=destin(l,5)-origin(l,5);
                if physical
                    dX=dX*spX;
                    dY=dY*spY;
                    dZ=dZ*spZ;
                end
                
                if weights
                    ww=WeigthIm(origin(l,3), origin(l,4), origin(l,5));
                    fprintf(fidVF, '%s', [num2str(origin(l,1)) ';' num2str(ss) ';' num2str(origin(l,3)) ';' num2str(origin(l,4)) ';' num2str(origin(l,5)) ';' num2str(dX) ';' num2str(dY) ';' num2str(dZ) ';' num2str(ww)]);
                else
                    fprintf(fidVF, '%s', [num2str(origin(l,1)) ';' num2str(ss) ';' num2str(origin(l,3)) ';' num2str(origin(l,4)) ';' num2str(origin(l,5)) ';' num2str(dX) ';' num2str(dY) ';' num2str(dZ) ';' num2str(origin(l,6))]);
                    
                end
                fprintf(fidVF, '\n');
            end
        end
        
        if exportVFinverted
            %Format of VF inverse
            pos=[outfoldervf datasetN stamp_ini ch snodetag '.csv']
            lpos=[outfoldervf datasetN stampnext ch snodetag '.csv']
            origin=dlmread(pos,';');
            destin=dlmread(lpos,';');
            ss= destin(1,2)-step
            for l=1:size(origin,1)
                dX=destin(l,3)-origin(l,3);
                dY=destin(l,4)-origin(l,4);
                dZ=destin(l,5)-origin(l,5);
                %if physical
                dX=-dX*spX;
                dY=-dY*spY;
                dZ=-dZ*spZ;
                %end
                fprintf(fidVFi, '%s', [num2str(origin(l,1)) ';' num2str(ss) ';' num2str(origin(l,3)) ';' num2str(origin(l,4)) ';' num2str(origin(l,5)) ';' num2str(dX) ';' num2str(dY) ';' num2str(dZ) ';' num2str(origin(l,6))]);
                fprintf(fidVFi, '\n');
            end
        end
        
        if exportDeform
            %Easy exportation of descriptors of the tensor for the GUI (deprecated)
            tensorfile=[outfoldertensor dataset stampref ch '_deform.tff'] %Tensor file format .tff
            %if t<tfinal
            sparseTensorFile2descriptorsFile(tensorfile, fidDF, exportStrain, axisProj);
            %end
        end
        
        if exportStrains
            %Easy exportation of descriptors of the tensor for the GUI (deprecated)
            tensorfile=[outfoldertensor dataset stampref ch '_strains.tff']
            %if t<tfinal
            sparseTensorFile2descriptorsFile(tensorfile, fidDF, 0, axisProj);
            %end
        end
        
        if exportEuler
            tensorfile=[outfoldertensor dataset stampref ch '_deform.tff'] %Tensor file format .tff
            %if t<tfinal
            %sparseTensorFile2descriptorsFile2D(tensorfile, fidDF, exportStrain, axisProj);
            sparseTensorFile2EulerdescriptorsFile(tensorfile, fidDF, t, axisProj);
            sparseTensorFile2EulerDescriptorsFile_v2(tensorfile, fidDF_v2, t);
            %end
        end
        
        if exportEulerTrack
            tensorfile=[outfoldertracktensors dataset stampref ch '_deform.tff'] %Tensor file format .tff
            %if t<tfinal
            %sparseTensorFile2descriptorsFile2D(tensorfile, fidDF, exportStrain, axisProj);
            sparseTensorFile2EulerDescriptorsFile(tensorfile, fidDFt, t, axisProj);
            sparseTensorFile2EulerDescriptorsFile_v2(tensorfile, fidDFt_v2, t);
            %end
        end
        
        if exportLag
            tensorfile=[outfoldertracktensors dataset stampref ch '_deform.tff'] %Tensor file format .tff
            if t==tini
                trackfile=[outfoldertrack dataset stampref ch '.csv']
                firstframe=dlmread(trackfile,';');
                size(firstframe)
                min(firstframe(:,1))
                maxf=max(firstframe(:,1))
                AcF=zeros(maxf,14);
                AcFe=zeros(maxf,14);
                for i=1:maxf
                    AcFe(i,5:13)=[1 0 0 0 1 0 0 0 1];
                    AcF(i,5:13)=[1 0 0 0 1 0 0 0 1];
                    AcFe(i,14)=0;
                    AcF(i,14)=0;
                end
            end
            %if the order is good
            %if t<tfinal
            Fall=readTensorFile(tensorfile);
            %fid fid=fopen(descriptorsFile, 'w');
            sfall=size(Fall)
            P=zeros(sfall(1));
            for e=1:size(Fall,1)
                %  x;y;z;t;tensor
                F=zeros(3,3);
                F(:)=AcF(e,5:13);
                Fin=Fall(e,:);
                Ft=zeros(3,3);
                Ft(:)= Fin(5:13);
                Fg=Ft*F;
                AcF(e,5:13)=Fg(:);
                AcFe(e,5:13)=Fg(:);
                AcFe(e,1:4)=Fin(1:4);
                AcF(e,14)=AcFe(e,14)+abs(trace(Ft));
                AcFe(e,14)=AcFe(e,14)+abs(trace(Ft));
                if t==tini
                    AcF(e,1:4)=Fin(1:4);
                end
            end
            %end
        end
    end
end

if exportLag
    exportLagDescriptorsFile(AcF, P, fidLF, axisProj);
    exportLagDescriptorsFile(AcFe, P, fidLFe, axisProj);
    %exportLagBackDescriptorsFile2D(AcF, fidLF, axisProj);
    %exportLagBackDescriptorsFile2D(AcFe, fidLFe, axisProj);
    fclose(fidLF);
    fclose(fidLFe);
end

if exportVF
    fclose(fidVF);
end

if (exportStrains || exportDeform)
    fclose(fidDF);
end

if exportEuler
    fclose(fidDF);
    fclose(fidDF_v2);
end

if exportEulerTrack
    fclose(fidDFt);
    fclose(fidDFt_v2);
end

if exportTrackingWorkflow
    fclose(fid);
    %saveMovitInfo(pathOut, dataset, steps, sin, sp, deltaTmilisecs, irodspath);
    %saveDatasetInfo(pathOut, dataset, steps, sin, sp, deltaThours, typeDataset);
end

% if exportVF2Movit
%
% end

if exportTracking
    Tracking=int32(Tracking);
    max(Tracking(:,3:5))
    min(Tracking(:,3:5))
    %fid=fopen(embfile,'w');
    dlmwrite(embfile, Tracking, 'delimiter', ';', 'precision', '%i');
    %save dataset info for movit and kinematics Benoit
    %if exportTracking
    %saveMovitInfo(pathOut, dataset, steps, sin, sp, deltaTmilisecs, irodspath);
    %saveDatasetInfo(pathOut, dataset, steps, sin, sp, deltaThours, typeDataset);
    %end
end