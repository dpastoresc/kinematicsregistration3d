% /* --------------------------------------------------------------------------------------
%  * File:    loadApps.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.

%execs mia (careful with debian or self installed)
setEnvC
path='/home/public/david/mia-build/src/'
path=''
exe_reg= [path 'mia-3dnonrigidreg'];
exe_regmulti = [path 'mia-3dnonrigidreg-alt'];
exe_track= [path 'mia-3dtrackpixelmovement'];
exe_tensor=[path 'mia-3deval-transformquantity'];
exe_reg2D= [path 'mia-2dimageregistration'];
exe_track2D=[path 'mia-2dtrackpixelmovement'];
exe_tensor2D=[path 'mia-2deval-transformquantity'];
%exe_strain='/home/public/mia/src/mia-3dtransformation-to-strain';
