% /* --------------------------------------------------------------------------------------
%  * File:    loadLibs.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.

%Importing libraries
mpath='matlab_code/';
%mpath='/home/public/matlab_code/';
addpath([mpath '/io/']);%my io functions
addpath([mpath '/io/NIFTI/']);%3rd party lib to handle nifti and analyze
addpath([mpath '/utils/']);%several utilities shared
addpath([mpath '/filter'])
addpath([mpath '/emgm'])
addpath([mpath '/autres'])
addpath(['libEMB/'])
addpath(['libMechanics/'])


