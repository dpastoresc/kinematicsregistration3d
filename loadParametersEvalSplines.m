% /* --------------------------------------------------------------------------------------
%  * File:    loadParametersEvalSplines.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.


loadParametersMIA;
configFileSystem

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Basic configuration
physical=0;%flag for physical values as results instead of pixels to evaluate the splines (needs to be checked)
% Options to have some metrics about registration quality (unstable)
weights=0
metricWeight=0
% The node spacing might be anisotropic for z dimension (look for type)
if type==1
    snode=[5 5 10];
elseif type==0
     snode=[10 10 10];
elseif type==2
    snode=[5 5 10];
else
    snode=5
end
snodetag=['_snode' num2str(snode)];

% IMPORTANT: Fordward registration->backward tracking
if forwards==1
    ttemp=tfinal;
    tfinal=tini;
    step=-step;
    tini=ttemp;
elseif forwards==2
    'not at the same time for 3D'
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% Initialization of seeds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%% SEEDS MODE!!! %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
seedsMode=0;%Check options below
% Predefined modes for biological structures
trackStructure=0;%1membrane 2nuclei, overwrites seedsMode if >0
% Options to create mask stuff->deprecated still used
backgroundThresholdRatio=0.02;%percenteage of the max value
doMask=1%apply-1 dismiss-0
mySeeds=''%if you set a mode where initial seeds are given
track_tag=''%here to specify a concrete tracking if several are generated 

%%%%%%%%%%%%%%%%%%%%%%%%%%% Initialization seeds options %%%%%%%%%%%%%%%%%%
% Predefined
if trackStructure==2    
    seedsMode=6;
    trackFolder='trackNuc'
elseif trackStructure==1
    seedsMode=5;
    trackFolder='track' 
else
    trackFolder='track'
end
%seedsMode options
%0- mesh all the image
%1- do a quick spot detection (quite bad for now) (using initial step raw)
%2- use centers VTK (set bioemergencesCentersVTK)
%3- use movit selection (set selection .csv)
%4- masked mesh having the mask from a file (set maskFile)
%5- used a downsampled mask from a file as seed for tracking! (set maskFile)
%6- using point markers to dilate as fake spots for initalization
%7- given points in a tracking format of this framework.
%8- squared grid
%9- veritcal lines
%10- horizontal lines
%11- Python segmentation (GMM)
%12- Fix mesh within a dynamic contour
%13- Contour mesh for tracking (similar to seedsMode=4)

if seedsMode==2
    bioemergencesCentersVTK=[folder dataset tstamp ch '_centers.vtk'];
end
if seedsMode==3
    %bioemergencesCentersCSV=%movit selection file .csv
end
if seedsMode>3 & seedsMode<8 
        maskFile=[datapathhome e '/' 'C1/binary/' dataset makeTimeStamp(tini) '_ch01' format] 
   if seedsMode==6
        maskFile=[datapathhome e '/' 'C1/z_Reconstruction/markers/' dataset makeTimeStamp(tini) '_ch01' format]
        %maskFile=[datapathhome e '/f_anisotropic/' dataset '_t000_ch00' format] 
   elseif seedsMode==7
        maskFile=mySeeds;%[datapathhome e '/trackTable/' dataset '_t000_ch01.csv']
   end
end
if seedsMode==8
   track_tag='_grid'%here to specify a concrete tracking... 
end
if seedsMode==9
   track_tag='_vert'%here to specify a concrete tracking... 
end
if seedsMode==10
   track_tag='_horz'%here to specify a concrete tracking... 
end
if seedsMode==11
   maskFile=mySeeds;%python generated centers
end
if seedsMode>11
   contourFolder=[datapathhome e '/' 'C1/contour/'] %dataset makeTimeStamp(tini) '_ch01' format] 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROCESING FLAGS. select how to evaluate your splines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

doSuperTracking=0;%create trajectories through deformation and correcting with morphology (NOT WORKING)
doVF=0;%Generate points transformed for a mesh -> outfoldervf
doDeform=0;%Calculate F for points given evaluating splines -> outfoldertensors
doStrains=0;%Calulate E for points given evaluating splins -> outfoldertensors
doTracking=1;%create trajectories through the displacements -> outfoldertrack
doDeformTrack=0;%calculate the tracking
if doTracking==0
    doDeformTrack=0
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% EXPORT FLAGS. puts the data into txt files for visualization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%COMPLEX DATA FORMATS
%EMB tracking exportation for movit
exportTracking=1
%Kind of EMB tracking for workflow
exportTrackingWorkflow=0
%FORMATS FOR DYNAMICS VISUALIZATION
exportVF=0;%__vf___
exportVFinverted=0
%Easy Tensor descriptors format for TensorVisualizer
exportDeform=0%eigenvalues of F (if exportStrains it becomes E). See axisProj vector
exportStrains=0%eigenvalues of E. See axisProj vector
exportEuler=0%like exportStrains plus p/q->>>>__eu__
%on the tracking
exportEulerTrack=0;%eulerian at tracking position->>>>__et___
exportLag=0;%lagrangian accumulating along trajectory->>>>___lg____ (beginning of trajectory) and ____lu___ (end of trajectory)
if trackStructure==2
    exportDeform=0
    exportStrains=0
    exportEuler=0
    exportVF=0
    expoertVFinverted=0
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% END USER INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PATH CONFIGURATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% All exportations are going to -> outfolder2
exportStrain=0;%process e from f
axisProj=[1 0 0];%dont touch

%setting paths... not touch
trackFolder=[trackFolder track_tag];
outfoldertrack= [outfolder2 trackFolder '/'];
outfoldervf= [outfolder2 'vf/'];
outfoldertensors= [outfolder2 'tensors/'];
outfoldertracktensors= [outfolder2 trackFolder '_tensors/'];
outfoldertrackLag= [outfolder2 trackFolder '_lag/'];
outfoldertrackLagTensors= [outfolder2 trackFolder '_lag_tensors/'];
%this is to overwrite the lag folder from particle trcking to mesh tracking
if seedsMode==0
    outfoldertrack=outfoldertrackLag;
    outfoldertracktensors=outfoldertrackLagTensors;
end
outfoldertensor=outfoldertensors;
mkdir(outfolder)%already created i think
mkdir(outfolder2)%already craeted i think
mkdir(outfoldertrack)
mkdir(outfoldervf)
mkdir(outfoldertensors)
mkdir(outfoldertracktensors)
% if exportLag
%     outfolderLCS=[outfolder2 'lcs/w' num2str(lapse-1) '/']%reaches tfinal-1
%     mkdir(outfolderLCS)
% end