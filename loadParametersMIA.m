% /* --------------------------------------------------------------------------------------
%  * File:    loadParametersMIA.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.

clear
loadLibs;
loadApps;

%Paramaters dataset
folder='../../Datasets/'
type=2;
dataset=''
tag=''
evalToo=0

%%% This are the parameters for MIA registration %%%%%
%%% opt is just the optimizer->check mia en sourceforge for details
%%% we specify also metric, bsplines spacing and levels of resolution
opt = 'nlopt:opt=ld-var2';
opt_param=',xtola=0,ftolr=0.01,maxiter=50' 
opt_tag='_ld-var2'
metric='image:cost=ssd'
levels=3;
cnode=20
cnode_p=num2str(cnode)
cnode_tag=cnode_p
anisotropy=0
alpha=100
degree=3
k_tag=['kernel=[bspline:d=' num2str(degree) '],']

if alpha>0
    penalty=['[divcurl:weight=' num2str(alpha) ']'];
    penalty_tag=['_dc-' num2str(alpha)];
   %% penalty=['[divcurl:div=' num2str(alpha) ']'];
   %% penalty_tag=['_div-' num2str(alpha)];
else
    penalty=''
    penalty_tag=''
end
if anisotropy
    cnode_p='[<30,30,60>]'
    cnode_tag='30-30-60'
end
%%%%%%%% Time-lapse dataset parameters %%%%%%%%%%%%%
tini=0%78
tfinal=100%80
step=1.0
channel=0
format='.hdr'
format='.vtk'

%%%%% FLAGS %%%%%
sim=0;%flag simulation
simtag=''%_15000avei'
forwards=0;%otherwise backwards. We want to register backwards to evaluate tracking forwards
%this is because the definition of the vector field x'=x-tv
doRegistration=1;%always on
applyTransformationVF=0;

if forwards==2
    evalToo=0
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%5 Naming configuration stuff
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ch=['_ch0' num2str(channel)]
%folder=[folder dataset '/' dataset tag ch '/']
folder=[folder dataset '/']
datasetN=[dataset tag];
infofile=[folder datasetN ch '_info.csv'];
opt_tag=[opt_tag penalty_tag]
