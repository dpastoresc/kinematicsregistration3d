% /* --------------------------------------------------------------------------------------
%  * File:    runMIA.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.

% Formats: 
%    - list of points(centers) 3D -> keep in .csv
%           id;t;x;y;z;reserved
%
%    - vector field sparse 3D -> keep in .csv
%           id;t;x;y;z;t;u;v;w;reserved
%
%    - tensor file format tff
%           MIA FORMAT check file formats documents and see header
%           readTensorFile.m (x,y,z,t) ({tensor})
%
%
% ToDO:
%   - multisource version   
%   - calculate trajectories by point and not by timestep
%   - ID of the trajectory is not preserved in the tensor file

% READ!!!! the registration from t->t+1 generates an image that converts
% the step t into an image more similar to t+1 (reference image)
% However!! the evaluation of pixel movements has to be done in the other
% way (inverse interpolation) so to track cells from t->t+n we need to
% register the sequence from t+n->t. The velocity field is defined here
% from t+1->t

% READ!!!! the registration from t+1->t generates an image that converts
% the step t+1 into an image more similar to t (reference image)
% However!! the evaluation of pixel movements has to be done in the other
% way (inverse interpolation) so thus we track cells from t->t+n

% IMPORTANT!!! 
% Convention:
    %forwards
    %t000->t001 generates t000.v3dt and t000_reg.image
    %backwards
    %We alter the order of tref and tin
    %t001->t000 generates t001.v3dt and t001_reg.image
%If we want to evaluate then forwards the tracking
    %t000 points use bwds/t001.v3dt to move to t001
%If tracking backwards
    %t001 points use fwds/t000.v3dt to move to t000

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loadParametersMIA;%change the file .m but do something more fashion next
loadLibs;
loadApps;
longPath=1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% AUTOMATED CODE %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%0%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%SAVE DATASET INO%%%%%%%%%%%%%%%%%%%
stampini = makeTimeStamp(tini);
namein=[folder datasetN stampini ch format];
[sin sp]=loadImageHeader(namein);
info=vertcat(sin, sp);
dlmwrite(infofile,info);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if forwards==1
    doF=1
    doB=0
elseif forwards==2
    doF=1
    doB=1
elseif forwards==0
    doF=0
    doB=1
end

for t=tini:step:(tfinal-1)
    
    %forwards
    %t000->t001 generates t000.v3dt and t000_reg.image
    if doF
        if sim           
                outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim_regFwd/'];
                outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim/bwd/'];          
        else         
                outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_regFwd/'];
                outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '/bwd/'];           
        end
        outfoldertemp= [outfolder ];%'reg/'];
        mkdir(outfolder)
        mkdir(outfoldertemp)       
            
        tref=t+step;
        stampin = makeTimeStamp(t);
        stampref = makeTimeStamp(tref);
        namein=[folder stampin format]
        nameref=[folder stampref format]
        namet=[outfoldertemp stampin opt_tag '.v3dt']
        nameout=[outfoldertemp stampin opt_tag '_reg' format]      
        %         if sim
        %             namein=[folder datasetN stampin ch  '_sim' simtag format]
        %             nameref=[folder datasetN stampref ch format]
        %             namet=[outfoldertemp datasetN stampin ch opt_tag '_sim' simtag '.v3dt']
        %             nameout=[outfoldertemp datasetN stampin ch opt_tag '_reg' format]
        %         else
        %             namein=[folder datasetN stampin ch format]
        %             nameref=[folder datasetN stampref ch format]
        %             namet=[outfoldertemp datasetN stampin ch opt_tag '.v3dt']
        %             nameout=[outfoldertemp datasetN stampin ch opt_tag '_reg' format]
        %         end
        %     end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Doing registration with mia
        if doRegistration        
            %the output is the bsplines file v3dt and the registred image. be
            %careful with the time step convention.

            if anisotropy
                splinerate=['"spline:anisorate=' cnode_p '" ']
            else
                splinerate=['spline:rate=' cnode_p]
            end 
            
            splinereg=''
            if strcmp(penalty,'')==0
                splinereg=[',penalty=' penalty]
            end
            
            command= [  exe_reg ' -i ' namein ...
                ' -r ' nameref ...
                ' -t ' namet ...
                ' -l ' num2str(levels) ...
                ' -f ' '"' splinerate splinereg '" ' metric...
                ' -O ' opt ...
                ' -o ' nameout   ]
            
            tic; system(command); toc
            %pause
        end
    end
    
    %backwards
    %We alter the order of tref and tin
    %t001->t000 generates t001.v3dt and t001_reg.image
    if doB
        %Generate fileystem
        %folder=[folder dataset '/' dataset tag ch '/']
        if sim           
                outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim_regBwd/'];
                outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_sim/fwd/'];
           
        else            
                outfolder=[folder 'miasplines_cnode' num2str(cnode) opt_tag '_regBwd/'];
                outfolder2=[folder 'miasplines_cnode' num2str(cnode) opt_tag '/fwd/'];
           
        end
        outfoldertemp= [outfolder ]%'reg/'];
        mkdir(outfolder)
        mkdir(outfoldertemp)

        tref=t+step;
        stampin = makeTimeStamp(tref);
        stampref = makeTimeStamp(t);
        
        if longPath
            namein=[folder datasetN stampin ch format]
            nameref=[folder datasetN stampref ch format]
            namet=[outfoldertemp datasetN stampin ch opt_tag '.v3dt']
            nameout=[outfoldertemp datasetN stampin ch opt_tag '_reg' format]
        else
            namein=[folder stampin format]
            nameref=[folder stampref format]
            namet=[outfoldertemp stampin opt_tag '.v3dt']
            nameout=[outfoldertemp stampin opt_tag '_reg' format]
        end
        
     
        if doRegistration        
            %the output is the bsplines file v3dt and the registred image. be
            %careful with the time step convention.
            if anisotropy
                splinerate=['"spline:anisorate=' cnode_p '" ']
                %splinerate=['spline:anisorate=' cnode_p]
            else
                splinerate=['spline:rate=' cnode_p]
            end

            splinereg=''
            if strcmp(penalty,'')==0
                splinereg=[',penalty=' penalty]
            end

            command= [  exe_reg ' -i ' namein ...
                        ' -r ' nameref ...
                        ' -t ' namet ...
                        ' -l ' num2str(levels) ...
                        ' -f ' '"' splinerate splinereg '" ' metric...
                        ' -O ' opt ...
                        ' -o ' nameout   ]                   
            tic; system(command); toc           
        end
    end
end  

if evalToo
   if forwards==0
       evaluateSplinesFwd
   else
       evaluateSplinesBwd
   end
end

%[ A sim sp]=loadImage(namein);
%[ A2 sim sp2]=loadImage(nameref);
%writeHDR_Byte(namein, A, [0.33 0.33 0.33], [0 0 0])
%writeHDR_Byte(nameref, A2, [0.33 0.33 0.33], [0 0 0])
%multisource version
%mia-3dnonrigidreg-alt -o reg.v3dt -l 2 -f spline:rate=3 image:cost=ssd,src=test.v,ref=ref.v divcurl:weight=10
%For one input is jut equivalent to mia-3dnonrigidreg. the
%registered image is not returned, only the transformation field
%of splines wiht -o (multiple outputs otherwise)
%here u can add more sources
%',src=' namein2 ',ref=' nameref ...
%command= [  exe_regmulti ' -t ' namet ...
%           ' -o ' namet ...
%           ' -l ' num2str(levels) ...
%           ' -f spline:rate=' num2str(cnode) ' ' metric ...
%           ',src=' namein ',ref=' nameref ...
%           ' -O ' opt ];

