% /* --------------------------------------------------------------------------------------
%  * File:    setEnvC.m
%  * Date:    01/09/2014
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.1
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2014, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM) and UCSD
%  All rights reserved.

%setenv('LD_LIBRARY_PATH', '/usr/lib/x86_64-linux-gnu:/lib/x86_64-linux-gnu:/usr/local/lib:/home/public/david/testmia/lib/x86_64-linux-gnu:/home/public/david/testmia/lib/x86_64-linux-gnu/mia-2.1/plugins')
%setenv('PATH', '/home/public/david/testmia/bin:$PATH')

setenv('LD_LIBRARY_PATH', '/usr/lib/x86_64-linux-gnu:/lib/x86_64-linux-gnu:/usr/local/lib:/home/public/david/testmia/lib/x86_64-linux-gnu:/home/public/david/testmia/lib/x86_64-linux-gnu/mia-2.1/plugins')
setenv('PATH', '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games')