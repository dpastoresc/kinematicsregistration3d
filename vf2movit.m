% /* --------------------------------------------------------------------------------------
%  * File:    flow2movit.m
%  * Date:    01/06/2015
%  * Author:  David Pastor Escuredo, david.pastor@upm.es
%  * Version: 0.2
%  * License: BSD
%  * --------------------------------------------------------------------------------------
%  Copyright (c) 2015, David Pastor Escuredo - david.pastor@upm.es
%  with Biomedical Image Technology, UPM (BIT-UPM)
%  All rights reserved.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LAGRANGE KINEMATICS DESCRIPTORS FROM TRACKING
% This version incrementes the temporal window T(t) from a reference point
% instead of moving a fixed window T

clear;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dNs=[1] %SET DATASET ID (datasetListBioEmergences.m)
process=1%just process (1) or export to movit (0)
xtag=''
real_pos=0
reuse_links=0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for dN=dNs
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tinilag=61%SET or use default (-1) in datasetListBioEmergences.m
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    loadParametersKinematics;
    tinilag
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%% READ ALL THE INTERMEDIATE DATA %%%%%%%%%%%%
    'Read Deformation Tensor Data'
    
    F=load([folder dataset '_' tensorid '.mat']);
    F = struct2cell(F);
    F=F{1};
    XYZ=load([folder dataset '_XYZ.mat']);
    XYZ = struct2cell(XYZ);
    XYZ=XYZ{1};
    cellid=load([folder dataset '_cellid.mat']);
    cellid = struct2cell(cellid);
    cellid=cellid{1};
    %cellnum=load_centData(dataset,'cellnum');
    cellnum=load([folder dataset '_cellnum.mat']);
    cellnum = struct2cell(cellnum);
    cellnum=cellnum{1};
    cellmother=load([folder dataset '_mother.mat']);
    cellmother = struct2cell(cellmother);
    cellmother=cellmother{1};
    cellchild=load([folder dataset '_child.mat']);
    cellchild = struct2cell(cellchild);
    cellchild=cellchild{1};
    Velo = load([folder dataset '_' veloid]);%load_centData(dataset, veloid);
    Velo = struct2cell(Velo);
    Velo=Velo{1};
    MeanVelo=load([folder dataset '_' velomeanid]);%load_centData(dataset, velomeanid);
    MeanVelo = struct2cell(MeanVelo);
    MeanVelo=MeanVelo{1};
    N=load([folder dataset '_' neighid]);%load_centData(dataset, neighid);
    N = struct2cell(N);
    N=N{1};
    
    %Initialization of data structure
    nl=33;
    stepcheck=32;%number of Ts accumulated so the integration is coherent
    
    %identity matrix
    I=[1 0 0; 0 1 0; 0 0 1];
    dt_step=0;
    if update_pos
        dt_step=dt_min;
    end
    DescriptorsL=[];
    DescriptorsLLeft=[];
    
    %Control Variables
    countNullTensorTotal=0
    debug_this=0
    
    %matlab indexes are 1-based
    tini_mat=tinilag+1;
    
    % Flow format
    % track_id;cellid;x;y;ztime_step;linkCorrect;childcell
    flow_file=[desc 'tracking-' num2str(tinilag) '-' num2str(tfinal) '-' num2str(real_pos) '-' num2str(reuse_links) '.csv']
    FF=dlmread(flow_file);
    %cellid;trackid;t
    'Creating flow for tracking'
    
    ids=sort(unique(FF(:,1)));
    steps=sort(unique(FF(:,6)));
    track=zeros([((length(ids)+1)*length(steps)+min(steps)) 9]);
    
    count=1
    for t=0:min(steps)-1
        track(count,6)=t;
        count=count+1;
    end
    %track(1:steps,:)
    %pause
    for t=steps'
        T=FF(FF(:,6)==t,:);
        track(count,6)=t;
        count=count+1;
        countT=1;
        for i=ids'
            track(count,1)=T(i,1);
            track(count,2)=countT;
            track(count,6)=t;
            m_xyz=T(i,3:5);
            m_xyz=round(m_xyz./spacingPixel);
            m_xyz(1:2)=m_xyz(1:2)-(sizePixel(1:2)/2);
            %m_xyz
            %pause
            track(count,3:5)=m_xyz;
            track(count,[7:9])=[i -1 countT];
            %track(1:count,:)
            %pause
            count=count+1;
            countT=countT+1;
            %pause
        end        
    end    
    flow_file=[desc 'tracking-' num2str(tinilag) '-' num2str(tfinal) '-' num2str(real_pos) '-' num2str(reuse_links) '.emb']
    dlmwrite(flow_file,track,';');
end
