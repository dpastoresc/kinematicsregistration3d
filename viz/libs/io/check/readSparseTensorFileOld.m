function A=readSparseTensorFileOld(file)
%David Pastor Escuredo 2012 
%Matlab function to read MIA sparse tensor files
%THIS IS AN OLD HEADER FORMAT!!!! last version includes dense and sparse tensor files

%in case you only want few pixels, you can use this format keeping an eye on the order or you can use the sparse tensor format
%(.sts extension) adding the x;y;z;t coordinates of each point and its
%tensor

%output A is a n elements x 13 table
%x;y;z;t;m0..m9

%file='../data/120518/S0205_ch00/miasplines_cnode10/S0205_t001_ch00_10_gd_gsstrains22.s'

%little endian
fid = fopen(file,'r', 'l');%Check endian

%Reading the header
%MIA
%tensorfield {
%  dim=3
%  components=9
%  repn=float32
%  size=1024 1024 258
%  endian=low
%}

fgetl(fid) % MIA
fgetl(fid) % tensorfield {
fgetl(fid) % DIM 3
fgetl(fid) % Components 9
fgetl(fid) % float32
fgetl(fid) % size 
fgetl(fid) % endian
fgetl(fid) % }
%fgets(fid,'\xC') % before

data=fread(fid, 'float32');
s=size(data)
numOfPoints=s(1)/13
for i=1:numOfPoints
index=i-1;
index=index*13+1;
A(i,:)=data(index:index+12);
end

