%To read fluid vtk vector field
function V= readVTK_VF(vtkfile)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Usage: V = readVTK(vtkfile)
%
%   V:       The matrix to be stored
%   vtkfile: The filename
%   notes:   Only reads binary STRUCTURED_POINTS
%
% David Pastor 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V = 0;

% open file (OBS! big endian format)
fid = fopen(vtkfile,'r');

if( fid == -1 )
  return
end

fgetl(fid) % # vtk DataFile Version x.x
fgetl(fid) % comments
fgetl(fid) % BINARY
fgetl(fid) % DATASET STRUCTURED_POINTS

s = fgetl(fid) % DIMENSIONS NX NY NZ
sz = sscanf(s, '%*s%d%d%d').'
%extract spacing
remain = s;
[token, remain] = strtok(remain);
dim = strread(remain);

fgetl(fid); % ORIGIN OX OY OZ
spacingtxt=fgetl(fid); % SPACING SX SY SZ
%extract spacing
remain = spacingtxt;
[token, remain] = strtok(remain);
spacing = strread(remain);

s2=fgetl(fid) % POINT_DATA NXNYNZ

%s = fgetl(fid) % SCALARS/VECTORS name data_type (ex: SCALARS imagedata unsigned_char)
svstr = sscanf(s2, '%s', 1)
dtstr = sscanf(s2, '%*s%*s%s')

V = fread(fid,3*prod(sz),'*float');
V = reshape(V,[3 sz]);

  
%V(1,1,1,1)
%V(2,1,1,1)
%V(3,1,1,1)

fclose(fid);