%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%Top level function for 3D
%supported: VTK, HDR, TIFF
%toDo... get the tiff info well... 

function [A sin sp]=loadImage(file)

    %hdr
    ext=file(length(file)-3:length(file))
    if (strcmp(ext,'.hdr'))
       [A sin sp] = readHDR(file);
    elseif (strcmp(ext,'.vtk'))
       [A sin sp origin]= readVTK(file);
    elseif (strcmp(ext,'.tif') || strcmp(ext,'.tif'))
        A=readTiff3D(ImName);
        info=iminfo(A);
        sin=size(A);
        sp=[1 1 1]
    else
        'Sorry, not supported'
    end