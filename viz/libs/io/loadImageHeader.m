%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%Top level function for 3D header
%ToDo: add TIFF. 

function [sin sp]=getImageHeader(file)

    [A sin sp] = loadImage(file);
   