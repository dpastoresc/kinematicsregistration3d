%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%relies on NIFTI library

function [A sin spin] = readHDR(fileName)
    addpath('NIFTI/');
    nii=load_nii(fileName);
    A=nii.img;
    sin=size(A);
    header=nii.hdr;
    spin=header.dime.pixdim(2:4);
    %spin=header.