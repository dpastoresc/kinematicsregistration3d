%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%just raw data type uchar

function out = readRaw(ImName, SizeMatrix)
    fid1 = fopen(ImName);
    in = fread(fid1);
    out = reshape(in, SizeMatrix);
    fclose(fid1);