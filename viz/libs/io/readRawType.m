%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%just raw data... specify pixel type

function out = readRawType(ImName, SizeMatrix, type)
    fid1 = fopen(ImName);
    in = fread(fid1, type);
    out = reshape(in, SizeMatrix);
    fclose(fid1);