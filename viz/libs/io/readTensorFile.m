%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%Reads MIA tensor files (2D works perfect, check for 3D)
%it will be nice to have the id of the trajectory in here

function A=readTensorFile(file)

%input:
%           -tensor file from MIA
%output:
%           -matrix with data (sparse vs grid)
%           in 3D
%               if sparse:  x;y;z;t;{9components}
%               if grid: {9components} for all pixels in the image...
%               ordered somehow :) (not working yet)

%little endian
fid = fopen(file,'r', 'l');%Check endian

%Reading the header
%MIA
%tensorfield {
%  dim=3
%  components=9/13
%  component=descriptor vector3, scalar, matirx3x3
%  elements=numer of elements with value
%  interpretation=gradient/strain
%  style=sparse/grid
%  precision=float32
%  sizeTransformation=1024 1024 258
%  endian=lo
%}

fgetl(fid); % MIA
fgetl(fid); % tensorfield {
[c v]=strtok(fgetl(fid),'='); % DIM 3
dim=str2num(v(2:length(v)));
[c v]=strtok(fgetl(fid),'='); % DIM 3
interpretation=v(2:length(v));
[c v]=strtok(fgetl(fid),'='); % DIM 3
precision=v(2:length(v));
[c v]=strtok(fgetl(fid),'='); % DIM 3
sizein=v(2:length(v));
[c v]=strtok(fgetl(fid),'='); % DIM 3
endian=v(2:length(v));
[c v]=strtok(fgetl(fid),'='); % DIM 3
components=str2num(v(2:length(v)));
[c v]=strtok(fgetl(fid),'='); % DIM 3
component_description=v(2:length(v));
[c v]=strtok(fgetl(fid),'='); % DIM 3
elements=str2num(v(2:length(v)));
[c v]=strtok(fgetl(fid),'='); % DIM 3
style=v(2:length(v));
fgetl(fid); % }
%fgets(fid,'\xC') % before

for i=1:(dim-1)
    [c left]=strtok(sizein,' ');
    sin(i)=str2num(c);
end
sin(dim)=str2num(left(2:length(left)));
%endian
%precision
machine='b';
if strcmp(endian,'low')
    machine='l';
end


%if(style.eq('sparse')
data=fread(fid, precision,0,machine);
    
if(strcmp(style, 'sparse'))
    
    s=size(data);
    numOfPoints=s(1)/components;
    A=zeros([numOfPoints components]);

    for i=1:numOfPoints
        
        index=(i-1)*components+1;
        %components
        %if i==1            
         %   values=data(index:index+(components-1),1)
       % else
            values=data(index:index+(components-1),1);
       % end
            
       
        %component_description
        %pause
        A(i,:)=values';
    end
%whole image. This is not going to work....
else          
    A=zeros(sin);
    index=i-1;
    index=index*components+1;
    A(i,:)=data(index:index+(components-1));
end
%data(1:9)
%A=reshape(data, [numOfPoints 9]);
%size(data2);
%data2(1,:);
