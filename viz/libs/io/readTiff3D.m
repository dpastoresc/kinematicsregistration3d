%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%Read tiff 3D, 2d image vector... z spacing is missing
%use loadImage better

function out = readTiff3D(ImName)
   
    info = imfinfo(ImName);
    num_images = numel(info);
    
    for k = 1:num_images
        out(:,:,k) = imread(ImName, k);% 'Info', info);
        % ... Do something with image A ...
    end