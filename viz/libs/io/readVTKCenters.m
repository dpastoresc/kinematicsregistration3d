%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%BIOEMERGENCES centers data format
%TEST! should be working. Download centers from bioemergences.

function V = readVTKCenters(vtkfile)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% David Pastor Escuredo 2012
% Read workflows center detection
% Mechanics framework
% 3D
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V = 0;

% open file (OBS! big endian format)
fid = fopen(vtkfile,'r');

if( fid == -1 )
  return
end

fgetl(fid); % # vtk DataFile Version x.x
fgetl(fid); % vtk output
fgetl(fid); % ASCII
fgetl(fid); % DATASET POLYDATA
s = fgetl(fid); % POINTS 5432 double

points = sscanf(s, '%*s%d').'
dtstr = sscanf(s, '%*s%*s%s')
sz=points*3;

 % V=fscanf(fid, '%s'); % the lookup table
  if( strcmp(dtstr,'unsigned_char') > 0 ) 
    % read data
    V = fread(fid,sz,'*uint8');
  V = reshape(V, [3 points]);
    V=V';
  elseif( strcmp(dtstr,'char') > 0 )
    % read data
    V = fread(fid,sz,'*int8');
   V = reshape(V, [3 points]);
    V=V';
  elseif( strcmp(dtstr,'unsigned_short') > 0 )
    % read data
    V = fread(fid,sz,'*uint16');
   V = reshape(V, [3 points]);
    V=V';
  elseif( strcmp(dtstr,'short') > 0 )
    % read data
    V = fread(fid,sz,'*int16');
   V = reshape(V, [3 points]);
    V=V';
  elseif( strcmp(dtstr,'unsigned_int') > 0 )
    % read data
    V = fread(fid,sz,'*uint32');
    V = reshape(V, [3 points]);
    V=V';
  elseif( strcmp(dtstr,'int') > 0 )
    % read data
    V = fread(fid,sz,'*int32');
   V = reshape(V, [3 points]);
    V=V';
  elseif( strcmp(dtstr,'float') > 0 )
    % read data
    V = fread(fid,sz,'*single');
    V = reshape(V, [3 points]);
    V=V';
  elseif( strcmp(dtstr,'double') > 0 )
    % read data
    V = fscanf(fid, '%f', sz);
    V = reshape(V, [3 points]);
    V=V';
  end


fclose(fid);