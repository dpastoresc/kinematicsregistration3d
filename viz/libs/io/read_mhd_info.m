%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%Code taken from Jose Luis Rubio

function [imSize pixel_size precision] = read_mhd_info(filepath, filename)

% Open the mhd file to get the element type and pixel size
mhd_name= [strtok(filename, '.') '.mhd'];
fid= fopen([filepath mhd_name]);
info= textscan(fid,'%s'); info= info{1};
fclose(fid);

% Find the pixel size
for m=1:length(info)
        if(strcmp(info{m},'ElementSpacing')); ps= m; break; end 
end
dx= str2num(info{ps+2});
dy= str2num(info{ps+3});
dz= str2num(info{ps+4});
pixel_size= [dx dy dz];

% Find the im size
for m=1:length(info)
        if(strcmp(info{m},'DimSize')); is= m; break; end 
end
a= str2num(info{is+2});
b= str2num(info{is+3});
c= str2num(info{is+4});
imSize= [a b c];

% Find the precision 
for m=1:length(info)
        if(strcmp(info{m},'ElementType')); p= m; break; end 
end
element_type= info{p+2};
switch lower(element_type)
    case 'met_char'
        precision= 'int8';
    case 'met_uchar'
        precision= 'uint8';
    case 'met_short'
        precision= 'int16';
    case 'met_ushort'
        precision= 'uint16';
    case 'met_int'
        precision= 'int32';
    case 'met_uint'
        precision= 'uint32';
    case 'met_float'
        precision= 'float';
    case 'met_double'
        precision= 'double';       
    % case 'int8=>int8'
    %     precision= 'int8'; 
    % case 'uint8=>uint8'
    %     precision= 'uint8'; % 'uint8=>uint8'
    % case 'int16=>int16'
    %     precision= 'int16';
    % case 'uint16=>uint16'
    %     precision= 'uint16';
    % case 'int32=>int32'
    %     precision= 'int32';
    % case 'uint32=>uint32'
    %     precision= 'uint32';
    % case 'float=>float'
    %     precision= 'float';
    % case 'double=>double'
    %     precision= 'double'; 
   otherwise
      disp('Unknown element type.')
end 









