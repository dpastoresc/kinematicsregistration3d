%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%This script converts hdr into 2D tiff sequence

pathLib='../David/matlab_code/'
addpath([pathLib 'io/'])
addpath([pathLib 'io/NIFTI/'])
addpath([pathLib 'utils/'])
%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%script hdr/VTK to tiffs
path=''
pattern='130206_E2B_t001_ch00'
imagename=[pattern '.hdr']
foldertif=[pattern '/']
mkdir(foldertif)
flag3D=1
flag2D=1

[A sin sp]=loadImage([path imagename]);
%pattern=imagename(1:length(imagename)-4)

%return
sin
sp
%spacing in tiff???!!!!

if flag2D
    for i=1:sin(3)
        
        imwrite(A(:,:,i), [foldertif pattern '_z' num2str(i-1) '.tif'], 'TIFF', 'Resolution', sp(1));
    end
end    
if flag3D
    for i=1:sin(3)
       imwrite(A(:, :, i), [pattern '.tif'], 'TIFF', 'WriteMode', 'append',  'Compression','none', 'Resolution', sp(1));
    end
    
end
