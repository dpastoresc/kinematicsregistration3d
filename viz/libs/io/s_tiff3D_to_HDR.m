%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%From 3D tiff to 3D HDR
%careful we dont believe in the spacing given by the tiff
%need to check how imaris handles the 2D seq spacing... shit format

pathLib='../David/matlab_code/'
addpath([pathLib 'io/'])
addpath([pathLib 'io/NIFTI/'])
addpath([pathLib 'utils/'])

c=0
path=['131024/ch0' num2str(c) '/']
%pattern='stack_ch00_z0000'
pattern=['stack_t000_ch0' num2str(c) ]
imagename=[pattern '.tif']
%foldertif=[pattern '/']


info=imfinfo([path imagename])
A=readTiff3D([path imagename]);
%size(A)
max(A(:))

%%%David: This is shit change of resolution... I should enhance the contrast
%%%somehow... or at least use the max for the window... so bad done so far. 
A2=Long2Byte(A);

%we save the spacing in microns!!
writeHDR_Byte([pattern '.hdr'], A2, [0.33 0.33 0.33], [0 0 0])
writeHDR_Long([pattern '_long.hdr'], uint16(A), [0.33 0.33 0.33], [0 0 0])
