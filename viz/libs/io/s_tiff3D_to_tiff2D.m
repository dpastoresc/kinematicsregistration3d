%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%3D images to 2D. not sure the purpose

clear all

DataFolder='/home/public/NicoleCBM/'
addpath('io/')
addpath('NIfTI_tools/')
for dataN=12:12
    for dataC=1:2

        if dataN==9

            name=['C' num2str(dataC) '-MAX_ECadTZip' num2str(dataN) '_asN.tif'] %solo MAX para algunos datasets!!!
            l1=22;

        elseif dataN==10 || dataN==11

            name=['C' num2str(dataC) '-MAX_ECadTZip' num2str(dataN) '_asN.tif'] %solo MAX para algunos datasets!!!
            l1=23;

        elseif dataN==6 || dataN==7 || dataN==8

            name=['C' num2str(dataC) '-ECadTZip' num2str(dataN) '_asN.tif']
            l1=18;
            
        elseif dataN==12
            name=['ECadGFPwt.tif']
        end


        if dataN < 10
            folder=['im0' num2str(dataN) '/']
            folder_channel=[folder 'im0' num2str(dataN) '_channel' num2str(dataC) '/']
            name_out=['00' num2str(dataN)]
        else
            folder=['im' num2str(dataN) '/']
            folder_channel=[folder 'im' num2str(dataN) '_channel' num2str(dataC) '/']
            name_out=['0' num2str(dataN)]
        end
        
        % OUT: analyze folder
        folder_ana=[DataFolder folder_channel 'timelapse/']
        mkdir(folder_ana)
        
        % IN: tiff folder
        tiffFile=[DataFolder folder_channel name]
        l=length(tiffFile);
        if dataN==12
            su=2
        else
            
        su=str2num(tiffFile(l-l1));
   
        end
        if su==dataC
            % IN: read tiff
            [image_org nsteps] = readTiff3D(tiffFile);
            image_org=im2double(image_org);
            

            for t=1:nsteps
                
                % IN: normalize tiff 2D
                image_t=image_org(:,:,t);
                image_t = 255*(image_t - min(image_t(:)))/(max(image_t(:))-min(image_t(:)));
                
                % OUT: analyze file 2D
                if t<11 %t=(1:10)-->t000:t009
                    name_out_t=[name_out '_t00' num2str(t-1) '_ch0' num2str(dataC)];   %formato ejemplo: 009_t001_ch00
                elseif t>100 %t=(101:end)-->t100:tend
                    name_out_t=[name_out '_t' num2str(t-1) '_ch0' num2str(dataC)];
                else %t=(11:100)-->t010:t099
                    name_out_t=[name_out '_t0' num2str(t-1) '_ch0' num2str(dataC)];
                end
                
                fileout_t=[folder_ana name_out_t]

                % OUT: create analyze
                %im_ana=make_ana(uint8(image_t'));
                %im_ana.hdr.dime.pixdim=spacing;
                %save_untouch_nii(im_ana,fileout_t);
                image=uint8(image_t);
                size(image)
                imwrite(image, [fileout_t '.tif'], 'tif');
                %X=imread([fileout_t '.tif']);
                %sX=size(X)
            end

        end
    end
end