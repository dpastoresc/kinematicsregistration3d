%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

clear all
DataFolder='/home/public/NicoleCBM/'
addpath('io/')
for dataN=8:8
    for dataC=1:2
        
        if dataN==9
            
            name=['C' num2str(dataC) '-MAX_ECadTZip' num2str(dataN) '_asN.tif'] %solo MAX para algunos datasets!!!
            name_diff=['C' num2str(dataC) '-MAX_ECadTZip' num2str(dataN) '_asN_diff.tif']
            l1=27;%l1=22;
            
        elseif dataN==10 || dataN==11
            
            name=['C' num2str(dataC) '-MAX_ECadTZip' num2str(dataN) '_asN.tif'] %solo MAX para algunos datasets!!!
            name_diff=['C' num2str(dataC) '-MAX_ECadTZip' num2str(dataN) '_asN_diff.tif']
            l1=28;%l1=23;
        
        elseif dataN==6 || dataN==7 || dataN==8
            
            name=['C' num2str(dataC) '-ECadTZip' num2str(dataN) '_asN.tif']
            name_diff=['C' num2str(dataC) '-ECadTZip' num2str(dataN) '_asN_diff.tif']
            l1=23;%l1=18;
            
        end
        
        
        if dataN < 10
            folder=['im0' num2str(dataN) '/']
            folder_channel=[folder 'im0' num2str(dataN) '_channel' num2str(dataC) '/']
            folder_diff=[folder 'DiffImages_0' num2str(dataC) '/']
            name_out=['im0' num2str(dataN) '_CH' num2str(dataC)]
            name_out_diff=['im0' num2str(dataN) '_CH' num2str(dataC) '_diff'];
            
        else
            folder=['im' num2str(dataN) '/']
            folder_channel=[folder 'im' num2str(dataN) '_channel' num2str(dataC) '/']
            folder_diff=[folder 'DiffImages_0' num2str(dataC) '/']
            name_out=['im' num2str(dataN) '_CH' num2str(dataC)]
            name_out_diff=['im' num2str(dataN) '_CH' num2str(dataC) '_diff']
        end


        %exp=folder(length(folder)-1:length(folder))

        %folderRec=[DataFolder folder 'RecImages_0' num2str(dataC) '/']

        %mkdir(folderRec)

        tiffFile=[DataFolder folder name]
        fileout=[DataFolder folder_channel name_out]
        fileout_diff=[DataFolder folder name_out]
        l=length(tiffFile);
        su=str2num(tiffFile(l-l1)); %cuando no hay MAX es l-18
        if su==dataC
            [image_org nsteps] = readTiff3D(tiffFile);
            image_org=im2double(image_org);

            image_fin=zeros(size(image_org,2), size(image_org,1));
            for i=1:nsteps
                image_t=image_org(:,:,i);
                image_t = 255*(image_t - min(image_t(:)))/(max(image_t(:))-min(image_t(:)));
                image_fin(:,:,i)=image_t';
            end
            image_fin=uint8(image_fin);
            dim=size(image_fin)
            
            writeVTK2(image_fin,dim,[fileout '.vtk'])

        end
    end
end