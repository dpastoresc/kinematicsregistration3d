function vtkPolyDataWrite(Vertex,Faces,Path,Colors)


[fid,Msg] = fopen(Path,'wt');

if fid == -1, error(Msg); end


%%% write VTK header %%%
fprintf(fid,'# vtk DataFile Version 2.0\n');
fprintf(fid,'Created by Matlab''s vtkPolyDataWriter\n');
fprintf(fid,'ASCII\n');
fprintf(fid,'DATASET POLYDATA\n');

nPoints = size(Vertex,1);
nFaces = size(Faces,1);

fprintf(fid,'POINTS %d float\n', nPoints);

for i=1:nPoints
    fprintf(fid, '%-.6f %-.6f %-.6f\n', Vertex(i,1), Vertex(i,2), Vertex(i,3));
end

fprintf(fid,'POLYGONS %d %d\n', nFaces, (4*nFaces));
for i=1:nFaces
    fprintf(fid, '3 %u %u %u\n', Faces(i,1), Faces(i,2), Faces(i,3));
end

if (exist('Colors','var'))
%     maxColor = max(Colors(:));
    fprintf(fid,'POINT_DATA %d\n',nPoints);
    fprintf(fid,'SCALARS vertex_colors float 1\n');
    fprintf(fid,'LOOKUP_TABLE default\n');
    for i=1:nPoints
        fprintf(fid,'%-.6f\n',Colors(i));
    end
end

fclose(fid);
end