function vtkVolumeWrite(Vol,Path,aRatio,origin,Colors)

    [fid,Msg] = fopen(Path,'wt');

    if fid == -1, error(Msg); end

    %%% write VTK header %%%
    fprintf(fid,'# vtk DataFile Version 2.0\n');
    fprintf(fid,'Created by Matlab''s vtkVolumeWriter\n');
    fprintf(fid,'ASCII\n');
    fprintf(fid,'DATASET STRUCTURED_POINTS\n');

    if (~exist('aRatio','var'))
        aRatio = [1,1,1];
    end
    if (~exist('origin','var'))
        origin = [1,1,1];
    end

    dims = size(Vol);

    fprintf(fid,'DIMENSIONS %d %d %d\n', size(Vol,1), size(Vol,2), size(Vol,3));
    fprintf(fid,'ASPECT_RATIO %d %d %d\n', aRatio(1), aRatio(2), aRatio(3));
    fprintf(fid,'ORIGIN %d %d %d\n', origin(1), origin(2), origin(3));

    nPoints = numel(Vol);
    fprintf(fid,'POINT_DATA %d\n',nPoints);
    fprintf(fid,'SCALARS volume_scalars unsigned_short 1\n');
    fprintf(fid,'LOOKUP_TABLE default\n');

    fprintf(fid, '%u ', uint16(Vol(:)));


    fclose(fid);
end