function vtkVolumeWriteBinary(Vol,Path,aRatio,origin,precision)

    [fid,Msg] = fopen(Path,'w');

    if fid == -1, error(Msg); end

    %%% write VTK header %%%
    fprintf(fid,'# vtk DataFile Version 3.0\n');
    fprintf(fid,'Created by Matlab''s vtkVolumeWriter\n');
    fprintf(fid,'BINARY\n');
    fprintf(fid,'DATASET STRUCTURED_POINTS\n');

    if (~exist('aRatio','var'))
        aRatio = [1,1,1];
    end
    if (~exist('origin','var'))
        origin = aRatio;
    end
    if (~exist('precision','var'))
        precision = 'uint16';
    end
% 
%     dims = size(Vol);

    fprintf(fid,'DIMENSIONS %d %d %d\n', size(Vol,1), size(Vol,2), size(Vol,3));
    fprintf(fid,'SPACING %f %f %f\n', aRatio(1), aRatio(2), aRatio(3));
    fprintf(fid,'ORIGIN %d %d %d\n', origin(1), origin(2), origin(3));

    nPoints = numel(Vol);
    fprintf(fid,'POINT_DATA %d\n',nPoints);

    % Hace falte el 'swapbytes' porque vtk por defecto asume BigEndian
    switch precision
        case 'uint8' 
            fprintf(fid,'SCALARS volume_scalars unsigned_char 1\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            fwrite (fid, uint8(Vol(:)), 'uint8');
        case 'uint16' 
            fprintf(fid,'SCALARS volume_scalars unsigned_short 1\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            fwrite (fid, swapbytes(uint16(Vol(:))), 'uint16');
        case 'single' 
            fprintf(fid,'SCALARS volume_scalars float 1\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            fwrite (fid, swapbytes(single(Vol(:))), 'float32');
        case 'double' 
            fprintf(fid,'SCALARS volume_scalars double 1\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            fwrite (fid, swapbytes(double(Vol(:))), 'double');
%     fprintf(fid, '%u ', uint16(Vol(:)));
    end

    fclose(fid);
end