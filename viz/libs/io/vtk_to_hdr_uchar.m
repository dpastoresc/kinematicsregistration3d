%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved


%vtk to hdr but only for uchar_


function vtk_to_hdr_uchar(filenameNoExt)

    in=[filenameNoExt '.vtk']
    out=[filenameNoExt '.hdr']
    
    [V dim spin origin]= readVTK(in);    
    dim
    spin
    origin
    writeHDR_Byte(out, V, spin, origin);