%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%Write image to Tiff
function write3D_to_2Dtiffs(path, pattern, A, sp, ztag)
% path output
% pattern 
% A matrix 3D
% 3D vector spacing
% ztag '_z'

sin=size(A)
    for i=1:sin(3)        
        imwrite(A(:,:,i), [path pattern ztag num2str(i-1) '.tif'], 'TIFF', 'Resolution', sp(1));
    end