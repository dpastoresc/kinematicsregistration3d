%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%Write image to Tiff

%carefull.. z pixel size is lost
function write3D_to_3Dtiff(filename, A,sp)
% filename
% A matrix 3D
% 3D vector spacing

sin=size(A);
    for i=1:sin(3)
       imwrite(A(:, :, i), filename, 'TIFF', 'WriteMode', 'append',  'Compression','none', 'Resolution', sp(1));
    end
     