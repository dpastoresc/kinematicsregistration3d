%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%One you have a grid as an image, just save it to a file

function writeGrid2D(outFileName, Mask)
    
    sizeIn=size(Mask);
    sx=sizeIn(1);
    sy=sizeIn(2);
   
    
    fid=fopen(outFileName, 'w');
    count=0
    for xi=1:sx
        for yi=1:sy
            
                if(Mask(xi,yi)>0)
                     fprintf(fid, '%s', [num2str(xi) ',' num2str(yi) ]); %',1')]);%see if 2D should be 3D with 1.
                     fprintf(fid, '\n');
                     count=count+1;
               
                end
        end
    end
    count
    fclose(fid);  
end
  

