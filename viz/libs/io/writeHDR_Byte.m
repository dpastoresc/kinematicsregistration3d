%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%write HDR images that are uchar only!! needs spacing and origin

function writeHDR_Byte(fileName, Matrix, spin, or)
    addpath('NIFTI/');
    ana = make_ana(Matrix, spin, or, 2);
    save_untouch_nii(ana, fileName);