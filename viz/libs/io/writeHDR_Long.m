%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%supposedly it writes hdr uint16, but i think this does not work

function writeHDR_Long(fileName, Matrix, spin, or)
    addpath('NIFTI/');
    ana = make_ana(Matrix, spin, or, 4);
    save_untouch_nii(ana, fileName);