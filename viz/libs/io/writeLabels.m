%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%write Labels as a raw image.
%very deprecated

function writeLabels(fileName, matrix)

%matrix=matrix*13;
%matrix = mod(matrix, nobjects);
fid=fopen(fileName, 'w+');
fwrite(fid, matrix, 'uint16');
fclose(fid);
