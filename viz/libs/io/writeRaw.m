%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%write raw, only uchar

function writeRaw(fileName, matrix)

fid=fopen(fileName, 'w+');
fwrite(fid, matrix, 'uint8');
fclose(fid);
