%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%write tiff 3D...
%to do: write the info image.

%just write raw specifying the type

function writeRawType(fileName, matrix, type)

fid=fopen(fileName, 'w+');
fwrite(fid, matrix, type);
fclose(fid);
