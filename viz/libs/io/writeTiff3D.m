%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved
%write tiff 3D...
%to do: write the info image.

function writeTiff3D(ImName, img)
   
    %info = imfinfo(ImName);
    %num_images = numel(info);
    
    for K=1:length(img(1, 1, :))
       imwrite(img(:, :, K), ImName,'TIFF', 'WriteMode', 'append',  'Compression','none', 'Resolution', sp);
    end
    
    