%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%write VTK vector field from matlab matrix ASCII format
%in this case we save vectors as SCALARS composed of 3 values...
function writeVTK_VF(u,v,w,Path,aRatio,origin,precision)

    [fid,Msg] = fopen(Path,'wt');

    if fid == -1, error(Msg); end

    %%% write VTK header %%%
    fprintf(fid,'# vtk DataFile Version 3.0\n');
    fprintf(fid,'Created by Matlab\n');
    fprintf(fid,'ASCII\n');
    fprintf(fid,'DATASET STRUCTURED_POINTS\n');

    if (~exist('aRatio','var'))
        aRatio = [1,1,1];
    end
    if (~exist('origin','var'))
        origin = aRatio;
    end
    if (~exist('precision','var'))
        precision = 'single';
    end
% 
%     dims = size(Vol);
    nx=size(u,1)
    ny=size(u,2)
    nz=size(u,3)
    
    fprintf(fid,'DIMENSIONS %d %d %d\n', nx, ny, nz);
    fprintf(fid,'SPACING %f %f %f\n', aRatio(1), aRatio(2), aRatio(3));
    fprintf(fid,'ORIGIN %d %d %d\n', origin(1), origin(2), origin(3));

    nPoints = numel(u);
    fprintf(fid,'POINT_DATA %d\n',nPoints);
    
    % Hace falte el 'swapbytes' porque vtk por defecto asume BigEndian
    switch precision
        case 'uint8' 
            %fprintf(fid,'SCALARS volume_scalars unsigned_char 1\n');
            %fprintf(fid,'LOOKUP_TABLE default\n');
            %fwrite (fid, uint8(Vol(:)), 'uint8');
                
            fprintf(fid, 'SCALARS vectors unsigned_char8 3\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fprintf(fid, '%u ', u(c,b,a));
                        fprintf(fid, '%u ', v(c,b,a));
                        fprintf(fid, '%u ', w(c,b,a));
                    end
                    fprintf(fid, '\n');
                end
            end
      
        case 'uint16' 
            fprintf(fid, 'SCALARS vectors unsigned_short 3\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fprintf(fid, '%u ', u(c,b,a));
                        fprintf(fid, '%u ', v(c,b,a));
                        fprintf(fid, '%u ', w(c,b,a));
                    end
                    fprintf(fid, '\n');
                end
            end
        case 'single' 
         fprintf(fid, 'SCALARS vectors float 3\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fprintf(fid, '%f ', u(c,b,a));
                        fprintf(fid, '%f ', v(c,b,a));
                        fprintf(fid, '%f ', w(c,b,a));
                    end
                    fprintf(fid, '\n');
                end
            end
        case 'double' 
            fprintf(fid, 'SCALARS vectors double 3\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fprintf(fid, '%f ', u(c,b,a));
                        fprintf(fid, '%f ', v(c,b,a));
                        fprintf(fid, '%f ', w(c,b,a));
                    end
                    fprintf(fid, '\n');
                end
            end
%     fprintf(fid, '%u ', uint16(Vol(:)));
    end

    fclose(fid);
end