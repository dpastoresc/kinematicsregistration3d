%matlab io image library 
%David Pastor Escuredo 2013 BIT-UPM 
%(c) All rights reserved

%write VTK vector field from matlab matrix binary format
%it tries to use VECTORS of VTK format... we use the version2 which is
%withs scalars within MIA
function writeVTK_VF_Binary(u,v,w,Path,aRatio,origin,precision)

    [fid,Msg] = fopen(Path,'w');

    if fid == -1, error(Msg); end

    %%% write VTK header %%%
    fprintf(fid,'# vtk DataFile Version 3.0\n');
    fprintf(fid,'Created by Matlab\n');
    fprintf(fid,'BINARY\n');
    fprintf(fid,'DATASET STRUCTURED_POINTS\n');

    if (~exist('aRatio','var'))
        aRatio = [1,1,1];
    end
    if (~exist('origin','var'))
        origin = aRatio;
    end
    if (~exist('precision','var'))
        precision = 'single';
    end
    
    precision
% 
%     dims = size(Vol);
    nx=size(u,1)
    ny=size(u,2)
    nz=size(u,3)
    
    fprintf(fid,'DIMENSIONS %d %d %d\n', nx, ny, nz);
    fprintf(fid,'SPACING %f %f %f\n', aRatio(1), aRatio(2), aRatio(3));
    fprintf(fid,'ORIGIN %d %d %d\n', origin(1), origin(2), origin(3));

    nPoints = numel(u);
    fprintf(fid,'POINT_DATA %d\n',nPoints);
    
    % Hace falte el 'swapbytes' porque vtk por defecto asume BigEndian
    % BECAREFUUUUUUUULL with the endian
    
    switch precision
        case 'uint8' 
            %fprintf(fid,'SCALARS volume_scalars unsigned_char 1\n');
            %fprintf(fid,'LOOKUP_TABLE default\n');
            %fwrite (fid, uint8(Vol(:)), 'uint8');
                
            fprintf(fid, 'VECTORS vectors unsigned_char8\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fwrite (fid, u(c,b,a), 'uint8');
                        fwrite (fid, v(c,b,a), 'uint8');
                        fwrite (fid, w(c,b,a), 'uint8');
                
                    end
                    fprintf(fid, '\n');
                end
            end
      
        case 'uint16' 
            fprintf(fid, 'VECTORS vectors unsigned_short\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fwrite (fid, u(c,b,a), 'uint16');
                        fwrite (fid, v(c,b,a), 'uint16');
                        fwrite (fid, w(c,b,a), 'uint16');                
                    end
                    fprintf(fid, '\n');
                end
            end
            
        case 'single' 
         fprintf(fid, 'VECTORS vectors float\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fwrite (fid, u(c,b,a), 'single');
                        fwrite (fid, v(c,b,a), 'single');
                        fwrite (fid, w(c,b,a), 'single');
                
                    end
                    fprintf(fid, '\n');
                end
            end
        case 'double' 
            fprintf(fid, 'VECTORS vectors double\n');
            fprintf(fid,'LOOKUP_TABLE default\n');
            %fprintf(fid, '\n');
            for a=1:nz
                for b=1:ny
                    for c=1:nx
                        fwrite (fid, u(c,b,a), 'double');
                        fwrite (fid, v(c,b,a), 'double');
                        fwrite (fid, w(c,b,a), 'double');
                
                    end
                    fprintf(fid, '\n');
                end
            end
%     fprintf(fid, '%u ', uint16(Vol(:)));
    end

    fclose(fid);
end