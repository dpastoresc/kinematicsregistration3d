function colorValue = getColor(value, maxVal, minVal)

    cMap=colormap(jet(255));
    mapLevels=size(cMap,1);       
    cValue=fix((value-minVal)/(maxVal-minVal)*mapLevels)+1;
     if cValue>size(cMap,1)
        cValue=size(cMap,1)
    end
    colorValue=cMap(cValue,:);
