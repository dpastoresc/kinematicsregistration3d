function [colorValue cValue]= getColorForMap(value, maxVal, minVal, cMap)


    mapLevels=size(cMap,1);
    levels=maxVal-minVal;
    cValue=fix(double((value-minVal)/double(levels))*mapLevels)+1;

    if cValue>size(cMap,1)
        cValue=size(cMap,1);
    end
     if cValue<1
        cValue=1;
    end
    colorValue=cMap(cValue,:);