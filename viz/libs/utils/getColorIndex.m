%Returns the value as colormpa index between 1 and the number of bins of
%the colormap
function cValue= getColorIndex(value, maxVal, minVal, mapLevels)

    levels=maxVal-minVal;
    cValue=fix(double((value-minVal)/double(levels))*(mapLevels-1))+1;

    if cValue>mapLevels
        cValue=mapLevels;
    end
    if cValue<1
        cValue=1;
    end
  