dirDatasets='datasets/z_vectorfields/';
type=2
physical=0
sim=0
C2='_15000avei'
simtag='_offsetL'
penalty='_dc-1'
exp='131121_E1'
if sim==0
    simtag=''
end
weights=1
cnode='40'
%cnode='30-30-60'
px=0
py=0
pz=0
if type==2
    node='5-5-10'
    leter='C'
    %scale=[0.42 0.42 0.25]
    scale=[0.25 0.25 0.25]
    scale=[0.25 0.25 0.25]
else
    node='10-10-10'
    leter='A'
     scale=[0.18 0.18 0.25]
end
session=[exp leter '_ch00_' cnode] 

    
if physical
    currentDataset=[session '_ld-var2' penalty '_vfphys_snode' node]  
else
    currentDataset=[session '_ld-var2' penalty '_vf_snode' node '']
end
if sim
    currentDataset= [currentDataset '_sim' num2str(C2)]
end


currentDataset=[currentDataset simtag]

if weights
 currentDataset= [currentDataset '_weights']
end

currentDataset_corrected=[currentDataset '_corrected.csv']
currentDataset_stat=[currentDataset '_stats.mat']
currentDataset=[currentDataset '.csv'];

vf = dlmread([dirDatasets currentDataset], ';');
size(vf);
vf(1,:);
%vf2=vf;

uPhys=vf(:,6:8);
uPhys(:,1)=uPhys(:,1)*scale(1);
uPhys(:,2)=uPhys(:,2)*scale(2);
uPhys(:,3)=uPhys(:,3)*scale(3);
if physical
   vf(6:8)=uPhys;
end
    
sx=max(vf(:,3)) 
sy=max(vf(:,4))
sz=max(vf(:,5))

limitx=[1+px*sx sx-px*sx]
limity=[1+py*sy sy-py*sy]
limitz=[1+pz*sz sz-pz*sz]

gridPoints=size(vf,1)
if px>0    
    ix=find((vf(:,3)<limitx(2)) & (vf(:,3)>limitx(1)));
    iy=find((vf(:,4)<limity(2)) & (vf(:,4)>limity(1)));
    iz=find((vf(:,5)<limitz(2)) & (vf(:,5)>limitz(1)));
    selected=size(ix,1);
    mx=mean(vf(ix,6));
    my=mean(vf(iy,7));
    mz=mean(vf(iz,8));
    maxx=prctile(abs(vf(ix,6)),95);
    maxy=prctile(abs(vf(iy,7)),95);
    maxz=prctile(abs(vf(iz,8)),95);
    maxx1=max(abs(vf(ix,6)));
    maxy1=max(abs(vf(iy,7)));
    maxz1=max(abs(vf(iz,8)));
 
else
    mx=mean(vf(:,6));
    my=mean(vf(:,7));
    mz=mean(vf(:,8));
    maxx=prctile(abs(vf(:,6)),95);
    maxy=prctile(abs(vf(:,7)),95);
    maxz=prctile(abs(vf(:,8)),95);
    maxx1=max(abs(vf(:,6)));
    maxy1=max(abs(vf(:,7)));
    maxz1=max(abs(vf(:,8)));
 
end

if physical
    max1s=[maxx1 maxy1 maxz1];
    maxs=[maxx maxy maxz];
    meds=[mx my mz];
else
    max1s=[maxx1 maxy1 maxz1].*scale;
    maxs=[maxx maxy maxz].*scale;
    meds=[mx my mz].*scale;
end
umod=sqrt(uPhys(:,1).^2+uPhys(:,2).^2+uPhys(:,3).^2);
    
stats=[mx my mz; maxx maxy maxz; maxx1 maxy1 maxz1];
stats(4,:)=meds;
stats(5,:)=maxs;
stats(6,:)=max1s;

vf(:,6)=vf(:,6)-mx;
vf(:,7)=vf(:,7)-my;
vf(:,8)=vf(:,8)-mz;
dlmwrite([dirDatasets currentDataset_corrected], vf, ';');
% save(['datasets/' currentDataset_stat], 'stats');
% stats

%stat2=dlmread(['datasets/' currentDataset_stat], ';')





