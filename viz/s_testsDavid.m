dirDatasets='datasets/z_vectorfields/';
type=1
physical=0
cnode='20'
sim=1
weights=1
C2=1500
if type
    node='5-5-10'
    leter='B'
    scale=[0.42 0.42 0.25]
else
    node='10-10-10'
    leter='A'
     scale=[0.18 0.18 0.25]
end
session=['130206_E2' leter '_ch00_' cnode] 
    
if physical
    currentDataset=[session '_ld-var2_vfphys_snode' node]  
else
    currentDataset=[session '_ld-var2_vf_snode' node '']
end
if sim
    currentDataset= [currentDataset '_sim' num2str(C2)]
end
if weights
 currentDataset= [currentDataset '_weights']
end
 currentDataset_corrected=[currentDataset '_negative.csv']
currentDataset_stat=[currentDataset '_stats.mat']
currentDataset=[currentDataset '.csv'];

vf = dlmread([dirDatasets currentDataset], ';');
vfsim=dlmread([dirDatasets '130206_E2B_ch00_20_ld-var2_vf_snode5-5-10_dipole1500_ave.csv'], ';');

size(vf);
vf(5000,:)
vfsim(5000,:)


vf(:,6)=-vf(:,6);
vf(:,7)=-vf(:,7);
vf(:,8)=-vf(:,8);
dlmwrite([dirDatasets currentDataset_corrected], vf, ';');





