%Vector Field Visualizer 2D-3D
%David Pastor Escuredo 
%(C) All rights reserved 2012-2013-2014
%BIT-UPM / MAE-UCSD
%ToDo 
%-more comparison options
%-Add strains descriptors in 3D
%-statistics of each descriptor
%-cross normalization (this was in the other visualizer i dont use anymore

function varargout = viewVF_nov13(varargin)
% VIEWIT MATLAB code for viewIt.fig
%      VIEWIT, by itself, creates a new VIEWIT or raises the existing
%      singleton*.
%
%      H = VIEWIT returns the handle to a new VIEWIT or the handle to
%      the existing singleton*.
%
%      VIEWIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VIEWIT.M with the given input arguments.
%
%      VIEWIT('Property','Value',...) creates a new VIEWIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before viewIt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to viewIt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help viewIt

% Last Modified by GUIDE v2.5 28-Nov-2013 00:19:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @viewVF_nov13_OpeningFcn, ...
                   'gui_OutputFcn',  @viewVF_nov13_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before viewIt is made visible.
function viewVF_nov13_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to viewIt (see VARARGIN)

% Choose default command line output for viewIt
handles.output = hObject;

% UIWAIT makes viewIt wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ALL SETTING UP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('libs/utils/');
handles.dirDatasets='datasets/z_vectorfields/'%120518/'
dD=dir(handles.dirDatasets)
numOfExperiments=size(dD,1)
dD(numOfExperiments)

ex=cell(1, 1);
ex{1}='None';
count=2;
for i=3:numOfExperiments
   n=dD(i).name;
   ex{count}=n;
   count=count+1;
end
size(ex)
set(handles.MenuVectorField, 'String', ex);%{'071226a','07});
set(handles.featureList, 'String', {'Module' 'xDisplacement' 'yDisplacement' 'zDisplacement' 'ModuleXY' 'Metric' 'Xpos' 'Xneg' 'YPos' 'YNeg' 'Zpos' 'Zneg'});
set(handles.datasetMenu2, 'String', ex);%{'071226a','07});
set(handles.featureMenu2, 'String', {'Module' 'xDisplacement' 'yDisplacement' 'zDisplacement' 'ModuleXY' 'Metric' 'Xpos' 'Xneg' 'YPos' 'YNeg' 'Zpos' 'Zneg'});

%%% HANDLES
handles.currentDataset='None';
handles.currentFeature='Module'
handles.old=0;

handles.currentDataset2='None';
handles.currentFeature2='Module'
%handles.vfield;

%handles.imSize=[1000 990 112];
%handles.pSize=[0.165 0.165 0.33];
% handles.imSize=[1024 1024 235];
% handles.pSize=[0.25 0.25 0.25];
% handles.imSize2=[1024 1024 235];
% handles.pSize2=[0.25 0.25 0.25];
handles.imSize=[0 0 0];
handles.pSize=[0 0 0];
handles.imSize2=[0 0 0];
handles.pSize2=[0 0 0];

handles.ratio=[1 1 1];%handles.pSize(1)./handles.pSize;
handles.ratio2=[1 1 1];%handles.pSize2(1)./handles.pSize2;
handles.stage=0;
handles.zslice=0;

handles.vmaxpr1=99;
handles.vmaxpr2=99;
handles.vminpr1=1;
handles.vminpr2=1;

handles.mode=0;
handles.bins=128;
handles.cmap=colormap(jet(128));
colorbar(handles.axesColormap);  
axes(handles.axesColormap);
axis off;
axes(handles.axes1);
axis off;
axes(handles.axes2);
axis off;
set(handles.axesColormap, 'XLim', [1 handles.bins]);

handles.timesteps=0;
handles.maxSlices=0;

handles.cell1=zeros(0)
handles.cell2=zeros(0)

handles.ax1=0;
handles.ax2=0;

handles.normalize=0;
handles.flagScale=1;
handles.scale=0.2;
handles.flagZvsXY=0;
handles.removeOffset=0;
set(handles.scaleFactor, 'String', num2str(handles.scale)); 
set(handles.scaleQuiver, 'Value', 1);
set(handles.seeVFFlag, 'Value',1);

set(handles.xs, 'String', num2str(handles.imSize(1) )); 
set(handles.ys, 'String', num2str(handles.imSize(2) )); 
set(handles.zs, 'String', num2str(handles.imSize(3) )); 
set(handles.pxs, 'String', num2str(handles.pSize(1) )); 
set(handles.pys, 'String', num2str(handles.pSize(2) )); 
set(handles.pzs, 'String', num2str(handles.pSize(3) )); 

set(handles.xs2, 'String', num2str(handles.imSize2(1) )); 
set(handles.ys2, 'String', num2str(handles.imSize2(2) )); 
set(handles.zs2, 'String', num2str(handles.imSize2(3) )); 
set(handles.pxs2, 'String', num2str(handles.pSize2(1) )); 
set(handles.pys2, 'String', num2str(handles.pSize2(2) )); 
set(handles.pzs2, 'String', num2str(handles.pSize2(3) )); 

set(handles.maxpr1, 'String', num2str(handles.vmaxpr1 )); 
set(handles.maxpr2, 'String', num2str(handles.vmaxpr2 )); 
set(handles.minpr1, 'String', num2str(handles.vminpr1 )); 
set(handles.minpr2, 'String', num2str(handles.vminpr2 )); 
%handles.desc=ones([10000 5])*(-1);
guidata(hObject, handles);




% --- Outputs from this function are returned to the command line.
function varargout = viewVF_nov13_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on selection change in MenuVectorField.
function MenuVectorField_Callback(hObject, eventdata, handles)
% hObject    handle to MenuVectorField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns MenuVectorField contents as cell array
%        contents{get(hObject,'Value')} returns selected item from MenuVectorField
string= get(handles.MenuVectorField,'String');
value= get(handles.MenuVectorField,'Value');
handles.currentDataset = string{value};
handles.ax1=1;
guidata(hObject, handles);

%Need to se if i should cache things or not
if(strcmp(handles.currentDataset,'None')==0)
    'Loading vector field'
    if handles.old==0
        h2=loadVectorField2(hObject, handles);
    else
        h2=loadVectorFieldOld2(hObject, handles); 
    end
    update_gui(h2);
end


% --- Executes during object creation, after setting all properties.
function MenuVectorField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MenuVectorField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in featureList.
function featureList_Callback(hObject, eventdata, handles)
% hObject    handle to featureList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns featureList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from featureList
string= get(handles.featureList,'String');
value= get(handles.featureList,'Value')
handles.currentFeature = string{value};
guidata(hObject, handles);
update_gui(handles);

% --- Executes during object creation, after setting all properties.
function featureList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to featureList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in featureMenu2.
function featureMenu2_Callback(hObject, eventdata, handles)
% hObject    handle to featureMenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns featureMenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from featureMenu2
string= get(handles.featureMenu2,'String')
value= get(handles.featureMenu2,'Value')
handles.currentFeature2 = string{value};
guidata(hObject, handles);
update_gui(handles);


% --- Executes during object creation, after setting all properties.
function featureMenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to featureMenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in datasetMenu2.
function datasetMenu2_Callback(hObject, eventdata, handles)
% hObject    handle to datasetMenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns datasetMenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from datasetMenu2
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
string= get(handles.datasetMenu2,'String');
value= get(handles.datasetMenu2,'Value');
handles.currentDataset2 = string{value};
handles.ax2=1;
guidata(hObject, handles);

%Need to se if i should cache things or not
if(strcmp(handles.currentDataset2,'None')==0)
    'Loading vector field'
    if handles.old==0
        h2=loadVectorField2_for2(hObject, handles);
    else
        h2=loadVectorFieldOld2_for2(hObject, handles); 
    end
     update_gui(h2);

end

% --- Executes during object creation, after setting all properties.
function datasetMenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to datasetMenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.



% --- Executes on slider movement.
function sliderTime_Callback(hObject, eventdata, handles)
% hObject    handle to sliderTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

frame=get(handles.sliderTime, 'Value')
frame=uint16(frame);
handles.stage=frame;
set(handles.tedit, 'String', num2str(frame));
guidata(hObject, handles);
update_gui(handles);


% --- Executes during object creation, after setting all properties.
function sliderTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on slider movement.
function sliderZ_Callback(hObject, eventdata, handles)
% hObject    handle to sliderTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

slice=get(handles.sliderZ, 'Value')
slice=uint16(slice);
handles.zslice=slice;
set(handles.zedit, 'String', num2str(slice));
guidata(hObject, handles);
update_gui(handles);


% --- Executes during object creation, after setting all properties.
function sliderZ_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function tedit_Callback(hObject, eventdata, handles)
% hObject    handle to tedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tedit as text
%        str2double(get(hObject,'String')) returns contents of tedit as a double


% --- Executes during object creation, after setting all properties.
function tedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function zedit_Callback(hObject, eventdata, handles)
% hObject    handle to tedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tedit as text
%        str2double(get(hObject,'String')) returns contents of tedit as a double


% --- Executes during object creation, after setting all properties.
function zedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function dataAvailableLabel_Callback(hObject, eventdata, handles)
% hObject    handle to dataAvailableLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dataAvailableLabel as text
%        str2double(get(hObject,'String')) returns contents of dataAvailableLabel as a double


% --- Executes during object creation, after setting all properties.
function dataAvailableLabel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dataAvailableLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in RawButton.
function RawButton_Callback(hObject, eventdata, handles)
% hObject    handle to RawButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of RawButton

    handles.seeRaw=get(hObject,'Value');
    guidata(hObject, handles);
    
    % --- Executes on button press in seeCellFlag.
function seeCellFlag_Callback(hObject, eventdata, handles)
% hObject    handle to seeCellFlag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of seeCellFlag
    handles.seeCell=get(hObject,'Value');
    guidata(hObject, handles);
    update_gui(handles)
    
% --- Executes on button press in oldData.
function oldData_Callback(hObject, eventdata, handles)
% hObject    handle to oldData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of oldData
    handles.old=get(hObject,'Value');
    guidata(hObject, handles);   
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%% DATA FOR AXES1 %%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function handles= loadVectorField2(hObject, handles)
          
    handles.ax1=1;
    vf = dlmread([handles.dirDatasets handles.currentDataset], ';');
    size(vf)
    %d(:,1:5)=vf(:,1:5);%CelliD, step, Coordinates
    %d(:,6:8)=vf(:,6:8);
    module=((vf(:,6).^2+vf(:,7).^2+vf(:,8).^2)).^0.5;
    modulexy=((vf(:,6).^2+vf(:,7).^2)).^0.5;
    handles.vfield=vf(:,1:8);
    handles.vfield(:,9)=module;
    handles.vfield(:,10)=modulexy;
    %d(:,9)=module;
    handles.vfield(:,11)=vf(:,9);%metric
       
    mns=min(vf(:,2))
    mxs=max(vf(:,2))
    if mxs>handles.timesteps
        handles.timesteps=mxs
        set(handles.sliderTime, 'Max', mxs); 
        set(handles.sliderTime, 'Min', mns); 
        if (mxs-mns)>0
            step=double(1/(mxs-mns))
            set(handles.sliderTime, 'SliderStep', [step; step]); 
        else
            step=1;
            set(handles.sliderTime, 'SliderStep', [step; step]); 
        end
        if(handles.stage==0)
            handles.stage=mns;
        end  
        set(handles.tedit, 'String', num2str(handles.stage)); 
        set(handles.sliderTime, 'Value', handles.stage); 
    end
    
          
    mnt=min(vf(:,5))
    mxt=max(vf(:,5))
    
    if mxt>handles.maxSlices
        handles.maxSlices=mxt;
        if(mxt==1)
            handles.zslice=1;       
        else
            step=double(1/(mxt-mnt))
            set(handles.sliderZ, 'Max', mxt); 
            set(handles.sliderZ, 'Min', mnt); 
            set(handles.sliderZ, 'SliderStep', [step; step]); 
            %Set zslice
            if(handles.zslice==0)
                handles.zslice=mnt;
            end
            set(handles.zedit, 'String', num2str(handles.zslice));    
            set(handles.sliderZ, 'Value', handles.zslice);
        end
    end
    handles.max1(1:5)=prctile(handles.vfield(:,1:5),handles.vmaxpr1);
    handles.min1(1:5)=prctile(handles.vfield(:,1:5),handles.vminpr1);
    handles.max1(6:8)=prctile(abs(handles.vfield(:,6:8)),handles.vmaxpr1);
    handles.min1(6:8)=prctile(abs(handles.vfield(:,6:8)),handles.vminpr1);
    handles.max1(9:11)=prctile(handles.vfield(:,9:11),handles.vmaxpr1);
    handles.min1(9:11)=prctile(handles.vfield(:,9:11),handles.vminpr1);
    %prctile(handles.vfield,99)
    %prctile(handles.vfield,1)
    %maxmodule=handles.max1(9)
    %maxmodule2=prctile(handles.vfield(:,9),98)
    
       
    guidata(hObject, handles);
    clear vf;
            
 %Draw descriptors by colors and not by samples 
 function drawVectorField_for1(handles)
     
    handles.vmaxpr1 = str2double(get(handles.maxpr1,'String'));
    handles.vminpr1 = str2double(get(handles.minpr1,'String'));
    %size(handles.vfield)
    %handles.stage
    set(gcf,'CurrentAxes',handles.axes1);
    idt= find(handles.vfield(:,2)==handles.stage);  
    sizetotal=size(handles.vfield)
    %isize=size(idt)
    dt=handles.vfield(idt, :);
    sizestep=size(dt)
    set(gcf,'Renderer','OpenGL');   
    val = get(handles.featureList,'Value');
    vfslice=find(dt(:,5)==handles.zslice);
    
    %Value selecting.
    i=9;
    if val==1
        i=9;
    elseif val==2
        i=6;
    elseif val==3
        i=7;
    elseif val==4
        i=8;
    elseif val==5
        'xy'
        i=10;
    elseif val==6
        i=11;
    elseif (val==7 || val==8)
        i=6;
    elseif (val==9 || val==10)
        i=7;
    elseif (val==11 || val==12)
        i=8;
    end
        
    %Normalize
    if handles.normalize==0
        if((handles.flagZvsXY)&&(i==8))
            mxval=prctile(abs(dt(:,9)),handles.vmaxpr1);%max(val);
            mnval=prctile(abs(dt(:,9)),handles.vminpr1);%min(val)
        else
            mxval=prctile(abs(dt(:,i)),handles.vmaxpr1);%max(val);
            mnval=prctile(abs(dt(:,i)),handles.vminpr1);%min(val)
        end
    else
        %We normalize with the values of the whole sequence!
        if((handles.flagZvsXY)&&(i==8))
            mxval=handles.max2(9);%max(val);
            mnval=handles.min2(9);%min(val);
        else
            mxval=handles.max1(val);%max(val);
            mnval=handles.min1(val);%min(val);
        end
    end
    set(handles.maxV1, 'String', num2str(mxval));
    set(handles.minV1, 'String', num2str(mnval));
    set(handles.editMax1Phys, 'String', num2str(mxval*handles.pSize(1)));
    set(handles.editMin1Phys, 'String', num2str(mnval*handles.pSize(1)));
    
    %Painting raw 
    paintingRaw=0
    if get(handles.seeCellFlag,'Value') && size(handles.cell1,1)>0
        cla
        hold on
        paintingRaw=1;
        m_g=gray(128);
        m_j=jet(128);
        cmapD = [m_g; m_j];
        handles.cmap=colormap(cmapD);
        
        if size(handles.cell1,3)>=handles.zslice
            'painting raw'
            im=handles.cell1(:,:,handles.zslice);
            im(im>(max(im(:))*0))=255;
            %im(im<255)=0;
            im=im';
            imagesc(im);
            colormap(gray(128));
        end
    else
        handles.cmap=getColorMap(handles);
    end
    
    
    if size(vfslice,1)>0
        set(handles.dataAvailableLabel, 'String', 'Data, wait while being displayed');
        %h1= findobj('Type','line');
        %delete(h1);  
        if paintingRaw==0
        cla;
        hold on          
        else
            paintingRaw=0
        end
        dz=dt(vfslice,:);
        if(val==7 || val==9 || val==11)
            c=dz(:,i);
            c(c<0)=0;
        elseif (val==8 || val==10 || val==12)
            c=dz(:,i);
            c(c>0)=0;
            c=abs(c);
        else
            c=abs(dz(:,i));
        end
             
        if get(handles.seeVFFlag,'Value')
        for i=1:size(dz,1)
            cindex=getColorIndex(c(i,1), mxval, mnval, handles.bins);
            ci(i,1)=cindex;        
        end    

        %We print by colors instead of by pixels.
        levels=max(ci(:,1))
        for ii=1:levels
           itc=find(ci(:,1)==ii); 
           if get(handles.seeCellFlag, 'Value');
               ii=ii+128;%separate colormap
           end
           
           %mod=sqrt(dz(itc,6).*dz(itc,6)+dz(itc,7).*dz(itc,7));
           if val==1
                if(handles.flagScale)
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), handles.scale, 'Color', handles.cmap(ii,:));
                else
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), 'Color', handles.cmap(ii,:));
                end
           elseif val==5
                 if(handles.flagScale)
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), handles.scale, 'Color', handles.cmap(ii,:));
                else
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), 'Color', handles.cmap(ii,:));
                end
           else
                plot(dz(itc,3),dz(itc,4),'*', 'Color', handles.cmap(ii,:), 'MarkerSize', 3);
           end
        end
        end
       
        set(handles.axes1, 'DataAspectRatio', handles.ratio);
        axis off
        hold off

        %setCameraProps(target, position, angle, vector, handles);
        %cla;axis off;
        cameratoolbar;

    else
        if paintingRaw
            axis off
            hold off
        end
        set(handles.dataAvailableLabel, 'String', 'No vf data');   
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%DATA FOR AXIS 2%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 function handles= loadVectorField2_for2(hObject, handles)
        
    handles.ax2=1;
    vf = dlmread([handles.dirDatasets handles.currentDataset2], ';');
    size(vf)

    %d(:,1:5)=vf(:,1:5);%CelliD, step, Coordinates
    %d(:,6:8)=vf(:,6:8);
    module=(vf(:,6).^2+vf(:,7).^2+vf(:,8).^2).^0.5;
    modulexy=((vf(:,6).^2+vf(:,7).^2)).^0.5;
    handles.vfield2=vf(:,1:8);
    handles.vfield2(:,9)=module;
    handles.vfield2(:,10)=modulexy;
    %d(:,9)=module;
    handles.vfield2(:,11)=vf(:,9);%metric
   
    mns=min(vf(:,2));
    mxs=max(vf(:,2));
    if mxs>handles.timesteps
        handles.timesteps=mxs;
        set(handles.sliderTime, 'Max', mxs); 
        set(handles.sliderTime, 'Min', mns); 
        if mxs-mns>0
            step=double(1/(mxs-mns))
            set(handles.sliderTime, 'SliderStep', [step; step]); 
        end
        if(handles.stage==0)
            handles.stage=mns;
        end  
        set(handles.tedit, 'String', num2str(handles.stage)); 
        set(handles.sliderTime, 'Value', handles.stage); 
    end
    
    mnt=min(vf(:,5))
    mxt=max(vf(:,5))
    if mxt>handles.maxSlices
        handles.maxSlices=mxt;
      if(mxt==1)
        handles.zslice=1;       
      else
        step=double(1/(mxt-mnt))
        set(handles.sliderZ, 'Max', mxt); 
        set(handles.sliderZ, 'Min', mnt); 
        set(handles.sliderZ, 'SliderStep', [step; step]); 
        %Set zslice
        if(handles.zslice==0)
            handles.zslice=mnt;
        end
        set(handles.zedit, 'String', num2str(handles.zslice));    
        set(handles.sliderZ, 'Value', handles.zslice); 
      end
    end
    handles.max2(1:5)=prctile(handles.vfield2(:,1:5),handles.vmaxpr2);
    handles.min2(1:5)=prctile(handles.vfield2(:,1:5),handles.vminpr2);
    handles.max2(6:8)=prctile(abs(handles.vfield2(:,6:8)),handles.vmaxpr2);
    handles.min2(6:8)=prctile(abs(handles.vfield2(:,6:8)),handles.vminpr2);
    handles.max2(9:11)=prctile(handles.vfield2(:,9:11),handles.vmaxpr2);
    handles.min2(9:11)=prctile(handles.vfield2(:,9:11),handles.vminpr2);
    guidata(hObject, handles);
    clear vf;
               
 function drawVectorField_for2(handles)
    %size(handles.vfield)
    %handles.stage

    handles.vmaxpr2 = str2double(get(handles.maxpr2,'String'));
    handles.vminpr2 = str2double(get(handles.minpr2,'String'));
    scale=handles.scale;
    %scale
    set(gcf,'CurrentAxes',handles.axes2);
    idt= find(handles.vfield2(:,2)==handles.stage);  
    sizetotal=size(handles.vfield2)
    isize=size(idt)
    dt=handles.vfield2(idt, :);
    set(gcf,'Renderer','OpenGL');   
    val = get(handles.featureMenu2,'Value');
    vfslice=find(dt(:,5)==handles.zslice);
    
    
        if val==1
            i=9;
        elseif val==2
            i=6;
        elseif val==3
            i=7;
           elseif val==4
            i=8;
        elseif val==5
            'xy'
            i=10; 
        elseif val==6
            i=11;
        elseif (val==7 || val==8)
            i=6;
        elseif (val==9 || val==10)
            i=7;            
        elseif (val==11 || val==12)
            i=8;            
        end
        if handles.normalize==0
            if((handles.flagZvsXY)&&(i==8))
                mxval=prctile(abs(dt(:,9)),handles.vmaxpr2);%max(val);
            	mnval=prctile(abs(dt(:,9)),handles.vminpr2);%min(val)  
            else 
            mxval=prctile(abs(dt(:,i)),handles.vmaxpr2);%max(val);
            mnval=prctile(abs(dt(:,i)),handles.vminpr2);%min(val)
            end
        else
         %We normalize with the values of the whole sequence!
            if((handles.flagZvsXY)&&(i==8))
                mxval=handles.max2(9);%max(val);
                mnval=handles.min2(9);%min(val);  
            else
            mxval=handles.max2(val);%max(val);
            mnval=handles.min2(val);%min(val);       
            end
        end     
    
    set(handles.maxV2, 'String', num2str(mxval));
    set(handles.minV2, 'String', num2str(mnval));
        
    set(handles.editMax2Phys, 'String', num2str(mxval*handles.pSize2(1)));
    set(handles.editMin2Phys, 'String', num2str(mnval*handles.pSize2(1)));    
    %'slice data HELLO'   
    %size(vfslice,1) 
    
     paintingRaw=0
        if get(handles.seeCellFlag,'Value') && size(handles.cell2,1)>0
            cla
            hold on
            paintingRaw=1;
            m_g=gray(128);
            m_j=jet(128);
            cmapD = [m_g; m_j];
            handles.cmap=colormap(cmapD);   
                  
           if size(handles.cell2,3)>=handles.zslice
               'painting raw'
               im=handles.cell2(:,:,handles.zslice);
               im(im>(max(im(:))*0))=255;
               %im(im<255)=0;
               im=im';
               imagesc(im);
               colormap(gray(128)); 
           end
        else
           handles.cmap=getColorMap(handles); 
        end
    
    
    
    if size(vfslice,1)>0
        set(handles.dataAvailableLabel, 'String', 'Data, wait while being displayed');
        %h1= findobj('Type','line');
        %delete(h1);  
        if paintingRaw==0
        cla;
        hold on          
        else
            paintingRaw=0;
        end
        dz=dt(vfslice,:);
        if(val==7 || val==9 || val==11)
            c=dz(:,i);
            c(c<0)=0;
        elseif (val==8 || val==10 || val==12)
            c=dz(:,i);
            c(c>0)=0;
            c=abs(c);
        else
            c=abs(dz(:,i));
        end
        
        handles.cmap=getColorMap(handles);
        
        for i=1:size(dz,1)
            cindex=getColorIndex(c(i,1), mxval, mnval, handles.bins);
            ci(i,1)=cindex;        
        end
               
        %We print by colors instead of by pixels.
        levels=max(ci(:,1))
        for ii=1:levels
           itc=find(ci(:,1)==ii);
           if val==1
                if(handles.flagScale)
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), handles.scale, 'Color', handles.cmap(ii,:));
                else
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), 'Color', handles.cmap(ii,:));
                end
           elseif val==5
                if(handles.flagScale)
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), handles.scale, 'Color', handles.cmap(ii,:));
                else
                    quiver(dz(itc,3), dz(itc,4), dz(itc,6), dz(itc,7), 'Color', handles.cmap(ii,:));
                end
           else
                plot(dz(itc,3),dz(itc,4),'*', 'Color', handles.cmap(ii,:), 'MarkerSize', 3);
           end
        end
        set(handles.axes2, 'DataAspectRatio', handles.ratio2);
        axis off
        hold off
        %setCameraProps(target, position, angle, vector, handles);
        %cla;axis off;       
    else
        if paintingRaw
            axis off
            hold off
        end
        set(handles.dataAvailableLabel, 'String', 'No data');   
    end
    cameratoolbar;
           
function update_gui(handles)  
   
    %handles=update_ranges(hObject, handles)
     if(handles.ax1==1)
         if get(handles.mode3D, 'Value')
            draw3D_for1(handles)
         else
            drawVectorField_for1(handles);
         end
     end
     if(handles.ax2==1)
         if get(handles.mode3D, 'Value')
            draw3D_for2(handles);
         else
            drawVectorField_for2(handles);
         end
     end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMAGE SETTINGS %%%%%%%%%%%%%%%%%%%%
function xs_Callback(hObject, eventdata, handles)
% hObject    handle to xs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xs as text
%        str2double(get(hObject,'String')) returns contents of xs as a double
handles.imSize(1) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function xs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ys_Callback(hObject, eventdata, handles)
% hObject    handle to ys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ys as text
%        str2double(get(hObject,'String')) returns contents of ys as a double
handles.imSize(2) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ys_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zs_Callback(hObject, eventdata, handles)
% hObject    handle to zs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zs as text
%        str2double(get(hObject,'String')) returns contents of zs as a double
handles.imSize(3) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function zs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pxs_Callback(hObject, eventdata, handles)
% hObject    handle to pxs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pxs as text
%        str2double(get(hObject,'String')) returns contents of pxs as a double


handles.pSize(1) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pxs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pxs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pys_Callback(hObject, eventdata, handles)
% hObject    handle to pys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pys as text
%        str2double(get(hObject,'String')) returns contents of pys as a double
handles.pSize(2) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pys_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pzs_Callback(hObject, eventdata, handles)
% hObject    handle to pzs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pzs as text
%        str2double(get(hObject,'String')) returns contents of pzs as a double
handles.pSize(3) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pzs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pzs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in tempReg.
function tempReg_Callback(hObject, eventdata, handles)
% hObject    handle to tempReg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tempReg

handles.normalize=get(hObject, 'Value');
guidata(hObject, handles);


% --- Executes on button press in scaleQuiver.
function scaleQuiver_Callback(hObject, eventdata, handles)
% hObject    handle to scaleQuiver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of scaleQuiver
handles.flagScale=get(hObject, 'Value');
guidata(hObject, handles);

function scaleFactor_Callback(hObject, eventdata, handles)
% hObject    handle to scaleFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of scaleFactor as text
%        str2double(get(hObject,'String')) returns contents of scaleFactor as a double
handles.scale = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function scaleFactor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scaleFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in zvsxy.
function zvsxy_Callback(hObject, eventdata, handles)
% hObject    handle to zvsxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of zvsxy
handles.flagZvsXY=get(hObject, 'Value');
guidata(hObject, handles);

function maxV1_Callback(hObject, eventdata, handles)
% hObject    handle to maxV1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxV1 as text
%        str2double(get(hObject,'String')) returns contents of maxV1 as a double


% --- Executes during object creation, after setting all properties.
function maxV1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxV1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minV1_Callback(hObject, eventdata, handles)
% hObject    handle to minV1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minV1 as text
%        str2double(get(hObject,'String')) returns contents of minV1 as a double


% --- Executes during object creation, after setting all properties.
function minV1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minV1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxV2_Callback(hObject, eventdata, handles)
% hObject    handle to maxV2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxV2 as text
%        str2double(get(hObject,'String')) returns contents of maxV2 as a double


% --- Executes during object creation, after setting all properties.
function maxV2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxV2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minV2_Callback(hObject, eventdata, handles)
% hObject    handle to minV2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minV2 as text
%        str2double(get(hObject,'String')) returns contents of minV2 as a double


% --- Executes during object creation, after setting all properties.
function minV2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minV2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function xs2_Callback(hObject, eventdata, handles)
% hObject    handle to xs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xs2 as text
%        str2double(get(hObject,'String')) returns contents of xs2 as a double
handles.imSize2(1) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function xs2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ys2_Callback(hObject, eventdata, handles)
% hObject    handle to ys2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ys2 as text
%        str2double(get(hObject,'String')) returns contents of ys2 as a double
handles.imSize2(2) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function ys2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ys2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zs2_Callback(hObject, eventdata, handles)
% hObject    handle to zs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zs2 as text
%        str2double(get(hObject,'String')) returns contents of zs2 as a double
handles.imSize2(3) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function zs2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pxs2_Callback(hObject, eventdata, handles)
% hObject    handle to pxs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pxs2 as text
%        str2double(get(hObject,'String')) returns contents of pxs2 as a double
handles.pSize2(1) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pxs2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pxs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pys2_Callback(hObject, eventdata, handles)
% hObject    handle to pys2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pys2 as text
%        str2double(get(hObject,'String')) returns contents of pys2 as a double
handles.pSize2(2) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pys2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pys2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pzs2_Callback(hObject, eventdata, handles)
% hObject    handle to pzs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pzs2 as text
%        str2double(get(hObject,'String')) returns contents of pzs2 as a double
handles.pSize2(3) = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pzs2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pzs2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minpr2_Callback(hObject, eventdata, handles)
% hObject    handle to minpr2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minpr2 as text
%        str2double(get(hObject,'String')) returns contents of minpr2 as a double
handles.vminpr2 = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function minpr2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minpr2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxpr2_Callback(hObject, eventdata, handles)
% hObject    handle to maxpr2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxpr2 as text
%        str2double(get(hObject,'String')) returns contents of maxpr2 as a double
handles.vmaxpr2 = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function maxpr2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxpr2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minpr1_Callback(hObject, eventdata, handles)
% hObject    handle to minpr1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minpr1 as text
%        str2double(get(hObject,'String')) returns contents of minpr1 as a double
handles.vminpr1 = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function minpr1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minpr1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxpr1_Callback(hObject, eventdata, handles)
% hObject    handle to maxpr1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxpr1 as text
%        str2double(get(hObject,'String')) returns contents of maxpr1 as a double
handles.vmaxpr1 = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function maxpr1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxpr1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in colormapmenu.
function colormapmenu_Callback(hObject, eventdata, handles)
% hObject    handle to colormapmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns colormapmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from colormapmenu
update_gui(handles);

% --- Executes during object creation, after setting all properties.
function colormapmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to colormapmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in commonReg.
function commonReg_Callback(hObject, eventdata, handles)
% hObject    handle to commonReg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of commonReg

function cMap = getColorMap(handles)
        
    val = get(handles.colormapmenu,'Value');
    if val ==1     
       cMap = colormap(jet(handles.bins));
    elseif val == 2
       cMap = colormap(hot(handles.bins));
    elseif val == 3
       cMap = colormap(gray(handles.bins));
    elseif val == 4
       cMap = colormap(copper(handles.bins));
    elseif val == 5
       cMap = colormap(hsv(handles.bins));
    elseif val == 6
       cMap = colormap(autumn(handles.bins));
    elseif val == 7
       cMap = colormap(summer(handles.bins));
    elseif val == 8
       cMap = colormap(winter(handles.bins)); 
    end
    %handles.cmap=cMap;
    %guidata(hObject, handles);
    %update_gui(handles);



% --- Executes on button press in reload.
function reload_Callback(hObject, eventdata, handles)
% hObject    handle to reload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.ax1==1
    loadVectorField2(hObject, handles)
end
if handles.ax2==1
    loadVectorField2_for2(hObject, handles)
end
update_gui(handles)  


% --- Executes on button press in seeVFFlag.
function seeVFFlag_Callback(hObject, eventdata, handles)
% hObject    handle to seeVFFlag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of seeVFFlag
update_gui(handles)

% --- Executes on button press in mode3D.
function mode3D_Callback(hObject, eventdata, handles)
% hObject    handle to mode3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of mode3D
update_gui(handles);

% --- Executes on button press in offsetFlag.
function offsetFlag_Callback(hObject, eventdata, handles)
% hObject    handle to offsetFlag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of offsetFlag
handles.removeOffset=get(hObject,'Value');
guidata(hObject, handles);
update_gui(handles);



function editMax1Phys_Callback(hObject, eventdata, handles)
% hObject    handle to editMax1Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMax1Phys as text
%        str2double(get(hObject,'String')) returns contents of editMax1Phys as a double


% --- Executes during object creation, after setting all properties.
function editMax1Phys_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMax1Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMin1Phys_Callback(hObject, eventdata, handles)
% hObject    handle to editMin1Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMin1Phys as text
%        str2double(get(hObject,'String')) returns contents of editMin1Phys as a double


% --- Executes during object creation, after setting all properties.
function editMin1Phys_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMin1Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMax2Phys_Callback(hObject, eventdata, handles)
% hObject    handle to editMax2Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMax2Phys as text
%        str2double(get(hObject,'String')) returns contents of editMax2Phys as a double


% --- Executes during object creation, after setting all properties.
function editMax2Phys_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMax2Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMin2Phys_Callback(hObject, eventdata, handles)
% hObject    handle to editMin2Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMin2Phys as text
%        str2double(get(hObject,'String')) returns contents of editMin2Phys as a double


% --- Executes during object creation, after setting all properties.
function editMin2Phys_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMin2Phys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in browseRaw.
function browseRaw_Callback(hObject, eventdata, handles)
% hObject    handle to browseRaw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%Display GUI for selecting the .raw, .dcm or .dicom file
addpath('libs/io/')
addpath('libs/NIFTI/')
[FileName,PathName] = uigetfile({'*.hdr', 'ANALYZE (*.hdr)'; ...
    '*.raw', 'RAW files (*.raw)'; ...    
    '*.vtk', 'VTK (*.vtk)'; ...
    '*.tif', 'TIFF (3D) (*.tif)'; ...
    '*.*' 'All files'}, ...
    'Select Raw Image for right (ch01 cell)', ...
    'MultiSelect', 'off');

     nn=fullfile(PathName, char(FileName(1,:)))
     [Im, sizeim, spim]=loadImage(nn);
     handles.cell1=Im;
     set(handles.pxs, 'String', num2str(spim(1)));
     set(handles.pys, 'String', num2str(spim(2)));
     set(handles.pzs, 'String', num2str(spim(3)));
     set(handles.xs, 'String', num2str(sizeim(1)));
     set(handles.ys, 'String', num2str(sizeim(2)));
     set(handles.zs, 'String', num2str(sizeim(3)));
     
     handles.pSize=spim;
     handles.imSize=sizeim;
     %handles.ratio=0;%handles.pSize(1)./handles.pSize;
     handles.ratio=handles.pSize(1)./handles.pSize;
     
     %update dataset properties
     sizeim
     guidata(hObject, handles);
     update_gui(handles);
  


% --- Executes on button press in browseRaw2.
function browseRaw2_Callback(hObject, eventdata, handles)
% hObject    handle to browseRaw2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    [FileName,PathName] = uigetfile({ '*.hdr', 'ANALYZE (*.hdr)'; ...
    '*.raw', 'RAW files (*.raw)'; ...   
    '*.vtk', 'VTK (*.vtk)'; ...
    '*.tif', 'TIFF (3D) (*.tif)'; ...
    '*.*' 'All files'}, ...
    'Select Raw Image for right (ch01 cell)', ...
    'MultiSelect', 'off');
      nn=fullfile(PathName, char(FileName(1,:)))
     [Im, sizeim, spim]=loadImage(nn);
     handles.cell2=Im;
     
     %update dataset properties
     set(handles.pxs2, 'String', num2str(spim(1)));
     set(handles.pys2, 'String', num2str(spim(2)));
     set(handles.pzs2, 'String', num2str(spim(3)));
     set(handles.xs2, 'String', num2str(sizeim(1)));
     set(handles.ys2, 'String', num2str(sizeim(2)));
     set(handles.zs2, 'String', num2str(sizeim(3)));
     handles.pSize2=spim;
     handles.imSize2=sizeim;
     %handles.ratio=0;%handles.pSize(1)./handles.pSize;
     handles.ratio2=handles.pSize2(1)./handles.pSize2;
     guidata(hObject, handles);
     update_gui(handles);
  


% --- Executes on button press in refresh.
function refresh_Callback(hObject, eventdata, handles)
% hObject    handle to refresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
update_gui(handles);

function draw3D_for1(handles)
     
    handles.vmaxpr1 = str2double(get(handles.maxpr1,'String'));
    handles.vminpr1 = str2double(get(handles.minpr1,'String'));
    %size(handles.vfield)
    %handles.stage
    set(gcf,'CurrentAxes',handles.axes1);
    idt= find(handles.vfield(:,2)==handles.stage);  
    sizetotal=size(handles.vfield)
    dt=handles.vfield(idt, :);
    xyz=dt(:,3:5);
    indexes=inBoxIndices(handles, xyz);
    dt=dt(indexes,:);
    sizeinbox=size(indexes)
    if get(handles.Downsample,'Value');
        inSample=[1:str2double(get(handles.sampleRate,'String')):length(indexes)];
        dt=dt(inSample,:);
    end 
    if get(handles.offsetFlag, 'Value');
        dt(:,6:8)=removeMean(dt(:,6:8));
        module=sqrt(dt(:,6).^2+dt(:,7).^2+dt(:,8).^2);  
        modulexy=sqrt((dt(:,6).^2+dt(:,7).^2));
        %changes dinamically
        dt(:,9)=module;
        dt(:,10)=modulexy;
        
    end
    sizestep=size(dt)
    set(gcf,'Renderer','OpenGL');   
    val = get(handles.featureList,'Value');
        
    %selecting value and normalizing
    i=9;
    if val==1
        i=9;
    elseif val==2
        i=6;
    elseif val==3
        i=7;
    elseif val==4
        i=8;
    elseif val==5
        'xy'
        i=10;
    elseif val==6
        i=11;
    elseif (val==7 || val==8)
        i=6;
    elseif (val==9 || val==10)
        i=7;
    elseif (val==11 || val==12)
        i=8;
    end
    
    %Normalize. 
    %ToDo: Add common normalization
    if handles.normalize==0
        if((handles.flagZvsXY)&&(i==8))
            mxval=prctile(abs(dt(:,9)),handles.vmaxpr1);%max(val);
            mnval=prctile(abs(dt(:,9)),handles.vminpr1);%min(val)
        else
            mxval=prctile(abs(dt(:,i)),handles.vmaxpr1);%max(val);
            mnval=prctile(abs(dt(:,i)),handles.vminpr1);%min(val)
        end
    else
        %We normalize with the values of the whole sequence!
        if((handles.flagZvsXY)&&(i==8))
            mxval=handles.max1(9);%max(val);
            mnval=handles.min1(9);%min(val);
        else
            mxval=handles.max1(val);%max(val);
            mnval=handles.min1(val);%min(val);
        end
    end
    set(handles.maxV1, 'String', num2str(mxval));
    set(handles.minV1, 'String', num2str(mnval));
    set(handles.editMax1Phys, 'String', num2str(mxval*handles.pSize(1)));
    set(handles.editMin1Phys, 'String', num2str(mnval*handles.pSize(1)));
      
    %old
    %imod=find(module>prctile(module,handles.vmaxpr1));
    %module(imod)=prctile(module,handles.vmaxpr1);
    %module(imod)=prctile(module,handles.vmaxpr1);
    %dmodv=module(imod,:);
    %mxval=max(dmodv)
    %mnval=min(dmodv);
        
    %RAW DATA 3D!!! redo with mesh of cell 
    paintingRaw=0
    if get(handles.seeCellFlag,'Value') && size(handles.cell1,1)>0
        cl
        hold on
        paintingRaw=1;
        m_g=gray(128);
        m_j=jet(128);
        cmapD = [m_g; m_j];
        handles.cmap=colormap(cmapD);
        
        if size(handles.cell1,3)>=handles.zslice
            'painting raw'
            im=handles.cell1(:,:,handles.zslice);
            im(im>(max(im(:))*0))=255;
            %im(im<255)=0;
            im=im';
            imagesc(im);
            colormap(gray(128));
        end
    else
        handles.cmap=getColorMap(handles);
    end        
    if paintingRaw==0
        cla;
        hold on
    else
        paintingRaw=0
    end   
             
    if get(handles.seeVFFlag,'Value')
        %selecting value
        %dz=dt(vfslice,:);this is for 2D
        if(val==7 || val==9 || val==11)
            c=dt(:,i);
            c(c<0)=0;
        elseif (val==8 || val==10 || val==12)
            c=dt(:,i);
            c(c>0)=0;
            c=abs(c);
        else
            c=abs(dt(:,i));
        end
        
        %We dont paint the samples that dont have a minimum threshold
        th=str2double(get(handles.threshold,'String'))/100.0;
        imod=find(c>mxval*th);
        imodisze= size(imod)
        dt=dt(imod,:);
        c=c(imod,:);
        %maybe I should refactor mxval and mnval... have a look later.
        
        for i=1:size(dt,1)
            cindex=getColorIndex(c(i), mxval, mnval, handles.bins);
            ci(i,1)=cindex;        
        end   
         
        %We print by colors instead of by pixels.
        levels=max(ci(:,1))
        for ii=1:levels
           itc=find(ci(:,1)==ii); 
         % size(itc)
           if get(handles.seeCellFlag, 'Value');
               ii=ii+128;%separate colormap
           end
          % dmod(itc,3:5);
           plot3(dt(itc,3), dt(itc,4), dt(itc,5), '.', 'Color', handles.cmap(ii,:));
          
        end
     end
       
     set(handles.axes1, 'DataAspectRatio', handles.ratio);
     axis off
     hold off  
     %setCameraProps(target, position, angle, vector, handles);
     %cla;axis off;
     cameratoolbar;
     if paintingRaw
            axis off
            hold off
     end
     %set(handles.dataAvailableLabel, 'String', 'No vf data');   

function draw3D_for2(handles)
     
    handles.vmaxpr2 = str2double(get(handles.maxpr2,'String'));
    handles.vminpr2 = str2double(get(handles.minpr2,'String'));
    %size(handles.vfield)
    %handles.stage
    set(gcf,'CurrentAxes',handles.axes2);
    idt= find(handles.vfield2(:,2)==handles.stage);  
    sizetotal=size(handles.vfield2)
    dt=handles.vfield2(idt, :);
    xyz=dt(:,3:5);
    indexes=inBoxIndices2(handles, xyz);
    dt=dt(indexes,:);
    sizeinbox=size(indexes)
    if get(handles.Downsample,'Value');
        inSample=[1:str2double(get(handles.sampleRate,'String')):length(indexes)];
        dt=dt(inSample,:);
    end 
    if get(handles.offsetFlag, 'Value');
        dt(:,6:8)=removeMean(dt(:,6:8));
        module=sqrt(dt(:,6).^2+dt(:,7).^2+dt(:,8).^2);  
        modulexy=sqrt((dt(:,6).^2+dt(:,7).^2));
        %changes dinamically
        dt(:,9)=module;
        dt(:,10)=modulexy;
        
    end
    sizestep=size(dt)
    set(gcf,'Renderer','OpenGL');   
    val = get(handles.featureMenu2,'Value');
        
    %selecting value and normalizing
    i=9;
    if val==1
        i=9;
    elseif val==2
        i=6;
    elseif val==3
        i=7;
    elseif val==4
        i=8;
    elseif val==5
        'xy'
        i=10;
    elseif val==6
        i=11;
    elseif (val==7 || val==8)
        i=6;
    elseif (val==9 || val==10)
        i=7;
    elseif (val==11 || val==12)
        i=8;
    end
    
    %Normalize
    if handles.normalize==0
        if((handles.flagZvsXY)&&(i==8))
            mxval=prctile(abs(dt(:,9)),handles.vmaxpr2);%max(val);
            mnval=prctile(abs(dt(:,9)),handles.vminpr2);%min(val)
        else
            mxval=prctile(abs(dt(:,i)),handles.vmaxpr2);%max(val);
            mnval=prctile(abs(dt(:,i)),handles.vminpr2);%min(val)
        end
    else
        %We normalize with the values of the whole sequence!
        if((handles.flagZvsXY)&&(i==8))
            mxval=handles.max2(9);%max(val);
            mnval=handles.min2(9);%min(val);
        else
            mxval=handles.max2(val);%max(val);
            mnval=handles.min2(val);%min(val);
        end
    end
    set(handles.maxV2, 'String', num2str(mxval));
    set(handles.minV2, 'String', num2str(mnval));
    set(handles.editMax2Phys, 'String', num2str(mxval*handles.pSize2(1)));
    set(handles.editMin2Phys, 'String', num2str(mnval*handles.pSize2(1)));
      
    %old
    %imod=find(module>prctile(module,handles.vmaxpr1));
    %module(imod)=prctile(module,handles.vmaxpr1);
    %module(imod)=prctile(module,handles.vmaxpr1);
    %dmodv=module(imod,:);
    %mxval=max(dmodv)
    %mnval=min(dmodv);
        
    %RAW DATA 3D!!! redo with mesh of cell 
    paintingRaw=0
    if get(handles.seeCellFlag,'Value') && size(handles.cell1,1)>0
        cl
        hold on
        paintingRaw=1;
        m_g=gray(128);
        m_j=jet(128);
        cmapD = [m_g; m_j];
        handles.cmap=colormap(cmapD);
        
        if size(handles.cell1,3)>=handles.zslice
            'painting raw'
            im=handles.cell1(:,:,handles.zslice);
            im(im>(max(im(:))*0))=255;
            %im(im<255)=0;
            im=im';
            imagesc(im);
            colormap(gray(128));
        end
    else
        handles.cmap=getColorMap(handles);
    end        
    if paintingRaw==0
        cla;
        hold on
    else
        paintingRaw=0
    end   
             
    if get(handles.seeVFFlag,'Value')
        %selecting value
        %dz=dt(vfslice,:);this is for 2D
        if(val==7 || val==9 || val==11)
            c=dt(:,i);
            c(c<0)=0;
        elseif (val==8 || val==10 || val==12)
            c=dt(:,i);
            c(c>0)=0;
            c=abs(c);
        else
            c=abs(dt(:,i));
        end
        
        %We dont paint the samples that dont have a minimum threshold
        th=str2double(get(handles.threshold,'String'))/100.0;
        imod=find(c>mxval*th);
        imodisze= size(imod)
        dt=dt(imod,:);
        c=c(imod,:);
        %maybe I should refactor mxval and mnval... have a look later.
        
        for i=1:size(dt,1)
            cindex=getColorIndex(c(i), mxval, mnval, handles.bins);
            ci(i,1)=cindex;        
        end   
         
        %We print by colors instead of by pixels.
        levels=max(ci(:,1))
        for ii=1:levels
           itc=find(ci(:,1)==ii); 
         % size(itc)
           if get(handles.seeCellFlag, 'Value');
               ii=ii+128;%separate colormap
           end
          % dmod(itc,3:5);
           plot3(dt(itc,3), dt(itc,4), dt(itc,5), '.', 'Color', handles.cmap(ii,:));
          
        end
     end
       
     set(handles.axes2, 'DataAspectRatio', handles.ratio2);
     axis off
     hold off  
     %setCameraProps(target, position, angle, vector, handles);
     %cla;axis off;
     cameratoolbar;
     if paintingRaw
            axis off
            hold off
     end
     %set(handles.dataAvailableLabel, 'String', 'No vf data');   


%%%%%%%%%%%%%%%%%%%%%USELESS THINGS%%%%%%%%%%%%%%%%%%%%%%%%%%%
function threshold_Callback(hObject, eventdata, handles)
% hObject    handle to threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of threshold as text
%        str2double(get(hObject,'String')) returns contents of threshold as a double

% --- Executes during object creation, after setting all properties.
function threshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function xmin2_Callback(hObject, eventdata, handles)
% hObject    handle to xmin2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xmin2 as text
%        str2double(get(hObject,'String')) returns contents of xmin2 as a double

% --- Executes during object creation, after setting all properties.
function xmin2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xmin2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function xmax2_Callback(hObject, eventdata, handles)
% hObject    handle to xmax2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xmax2 as text
%        str2double(get(hObject,'String')) returns contents of xmax2 as a double

% --- Executes during object creation, after setting all properties.
function xmax2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xmax2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ymin2_Callback(hObject, eventdata, handles)
% hObject    handle to ymin2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ymin2 as text
%        str2double(get(hObject,'String')) returns contents of ymin2 as a double

% --- Executes during object creation, after setting all properties.
function ymin2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ymin2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ymax2_Callback(hObject, eventdata, handles)
% hObject    handle to ymax2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ymax2 as text
%        str2double(get(hObject,'String')) returns contents of ymax2 as a double

% --- Executes during object creation, after setting all properties.
function ymax2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ymax2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function zmin2_Callback(hObject, eventdata, handles)
% hObject    handle to zmin2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zmin2 as text
%        str2double(get(hObject,'String')) returns contents of zmin2 as a double

% --- Executes during object creation, after setting all properties.
function zmin2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zmin2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function zmax2_Callback(hObject, eventdata, handles)
% hObject    handle to zmax2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zmax2 as text
%        str2double(get(hObject,'String')) returns contents of zmax2 as a double

% --- Executes during object creation, after setting all properties.
function zmax2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zmax2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function xmin1_Callback(hObject, eventdata, handles)
% hObject    handle to xmin1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xmin1 as text
%        str2double(get(hObject,'String')) returns contents of xmin1 as a double

% --- Executes during object creation, after setting all properties.
function xmin1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xmin1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function xmax1_Callback(hObject, eventdata, handles)
% hObject    handle to xmax1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xmax1 as text
%        str2double(get(hObject,'String')) returns contents of xmax1 as a double

% --- Executes during object creation, after setting all properties.
function xmax1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xmax1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ymin1_Callback(hObject, eventdata, handles)
% hObject    handle to ymin1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ymin1 as text
%        str2double(get(hObject,'String')) returns contents of ymin1 as a double

% --- Executes during object creation, after setting all properties.
function ymin1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ymin1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ymax1_Callback(hObject, eventdata, handles)
% hObject    handle to ymax1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ymax1 as text
%        str2double(get(hObject,'String')) returns contents of ymax1 as a double

% --- Executes during object creation, after setting all properties.
function ymax1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ymax1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function zmin1_Callback(hObject, eventdata, handles)
% hObject    handle to zmin1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zmin1 as text
%        str2double(get(hObject,'String')) returns contents of zmin1 as a double

% --- Executes during object creation, after setting all properties.
function zmin1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zmin1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function zmax1_Callback(hObject, eventdata, handles)
% hObject    handle to zmax1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zmax1 as text
%        str2double(get(hObject,'String')) returns contents of zmax1 as a double

% --- Executes during object creation, after setting all properties.
function zmax1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zmax1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%box stuff
function indexes=inBoxIndices(handles, xyz)
    
    maxX=max(xyz(:,1));
    maxY=max(xyz(:,2));
    maxZ=max(xyz(:,3));
    minX=min(xyz(:,1));
    minY=min(xyz(:,2));
    minZ=min(xyz(:,3));
    maxX=maxX*str2double(get(handles.xmax1,'String'))/100.0;
    minX=minX+maxX*str2double(get(handles.xmin1,'String'))/100.0;
    maxY=maxY*str2double(get(handles.ymax1,'String'))/100.0;
    minY=minY+maxY*str2double(get(handles.ymin1,'String'))/100.0;
    maxZ=maxZ*str2double(get(handles.zmax1,'String'))/100.0;
    minZ=minZ+maxZ*str2double(get(handles.zmin1,'String'))/100.0;    
    [maxX maxY maxZ]
    [minX minY minZ]
    xcheck=(xyz(:,1)>=minX &  xyz(:,1)<=maxX);
    ycheck=(xyz(:,2)>=minY &  xyz(:,2)<=maxY);
    zcheck=(xyz(:,3)>=minZ &  xyz(:,3)<=maxZ);
    c=xcheck+ycheck+zcheck;
    indexes=find(c==3);
    %ssindexes= size(indexes)
        
function indexes=inBoxIndices2(handles, xyz)
    
    maxX=max(xyz(:,1));
    maxY=max(xyz(:,2));
    maxZ=max(xyz(:,3));
    minX=min(xyz(:,1));
    minY=min(xyz(:,2));
    minZ=min(xyz(:,3));
    maxX=maxX*str2double(get(handles.xmax2,'String'))/100.0;
    minX=minX+maxX*str2double(get(handles.xmin2,'String'))/100.0;
    maxY=maxY*str2double(get(handles.ymax2,'String'))/100.0;
    minY=minY+maxY*str2double(get(handles.ymin2,'String'))/100.0;
    maxZ=maxZ*str2double(get(handles.zmax2,'String'))/100.0;
    minZ=minZ+maxZ*str2double(get(handles.zmin2,'String'))/100.0;    
    [maxX maxY maxZ]
    [minX minY minZ]
    xcheck=(xyz(:,1)>=minX &  xyz(:,1)<=maxX);
    ycheck=(xyz(:,2)>=minY &  xyz(:,2)<=maxY);
    zcheck=(xyz(:,3)>=minZ &  xyz(:,3)<=maxZ);
    c=xcheck+ycheck+zcheck;
    indexes=find(c==3);
    %ssindexes= size(indexes)     


% --- Executes on button press in browseMesh1.
function browseMesh1_Callback(hObject, eventdata, handles)
% hObject    handle to browseMesh1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in browseMesh2.
function browseMesh2_Callback(hObject, eventdata, handles)
% hObject    handle to browseMesh2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in Downsample.
function Downsample_Callback(hObject, eventdata, handles)
% hObject    handle to Downsample (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Downsample
update_gui(handles);

% --- Executes on button press in saveVF1.
function saveVF1_Callback(hObject, eventdata, handles)
% hObject    handle to saveVF1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

vf=zeros(size(handles.vfield));
xyz=handles.vfield(:,3:5);
idvalid=inBoxIndices(handles, xyz)
vf(idvalid,:)=handles.vfield(idvalid,:);
if get(handles.offsetFlag, 'Value')
    vf(:,6:8)=removeMean(vf(:,6:8));
end
%uisave
[name,path] = uiputfile([handles.dirDatasets handles.currentDataset]);
vfsave=zeros(size(vf,1),9);
vfsave(:,1:8)=vf(:,1:8);%we dont save the module
%add the metric
vfsave(:,9)=vf(:,11);
dlmwrite([path name], vfsave, ';');


% --- Executes on button press in saveVF2.
function saveVF2_Callback(hObject, eventdata, handles)
% hObject    handle to saveVF2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vf=zeros(size(handles.vfield2));
xyz=handles.vfield2(:,3:5);
idvalid=inBoxIndices2(handles, xyz)
vf(idvalid,:)=handles.vfield2(idvalid,:);
if get(handles.offsetFlag, 'Value')
    vf(:,6:8)=removeMean(vf(:,6:8));
end
%uisave
[name,path] = uiputfile([handles.dirDatasets handles.currentDataset2]);
vfsave=zeros(size(vf,1),9);
vfsave(:,1:8)=vf(:,1:8);%we dont save the module
%add the metric
vfsave(:,9)=vf(:,11);
dlmwrite([path name], vfsave, ';');

    
function xyz=removeMean(xyz)
    xyz(:,1)=xyz(:,1)-mean(xyz(:,1));
    xyz(:,2)=xyz(:,2)-mean(xyz(:,2));
    xyz(:,3)=xyz(:,3)-mean(xyz(:,3));

function sampleRate_Callback(hObject, eventdata, handles)
% hObject    handle to sampleRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sampleRate as text
%        str2double(get(hObject,'String')) returns contents of sampleRate as a double


% --- Executes during object creation, after setting all properties.
function sampleRate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sampleRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in resize.
function resize_Callback(hObject, eventdata, handles)
% hObject    handle to resize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%update dataset properties))
     handles.imSize=[str2double(get(handles.xs, 'String')) str2double(get(handles.ys, 'String')) str2double(get(handles.zs, 'String'))];
     handles.imSize2=[str2double(get(handles.xs2, 'String')) str2double(get(handles.ys2, 'String')) str2double(get(handles.zs2, 'String'))];
     
     handles.imSize=[str2double(get(handles.pxs, 'String')) str2double(get(handles.pys, 'String')) str2double(get(handles.pzs, 'String'))];
     handles.pSize2=[str2double(get(handles.pxs2, 'String')) str2double(get(handles.pys2, 'String')) str2double(get(handles.pzs2, 'String'))];
     
   
     if sum(handles.pSize)>0
     handles.ratio=handles.pSize(1)./handles.pSize;
     end
     
     if sum(handles.pSize2)>0
     handles.ratio2=handles.pSize2(1)./handles.pSize2;
     end
     

% --- Executes on button press in makeMesh.
function makeMesh_Callback(hObject, eventdata, handles)
% hObject    handle to makeMesh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
A=handles.cell1;
A(A>0)=1;%binary, put threshold
